#!/usr/bin/lua

local entries = {
	--[[
	context_created = {
		arguments = { "parent_context_id", "context_type"}
	},
	context_closed = {
		arguments = {"most_significant_error"},
	},
	context_argument_set = {
		severity = "UPDATE",
		arguments = { "key", "value" }
	},
	context_attribute_set = {
		severity = "UPDATE",
		arguments = { "key", "value" }
	},
	context_most_significant_child_set = {
		arguments = { "most_significant_child" }
	},
	subrequest_made = {
		arguments = { "subrequest_uuid", "uri", "action", "resource_id" }
	},
	resource_created = {
		arguments = { "resource_id" }
	},
	resource_submitted = {
		arguments = { "resource_id", "uri" }
	},
	resource_dropped = {
		arguments = { "resource_id" }
	},
	resource_attribute_set = {
		severity = "UPDATE",
		arguments = { "resource_id", "key", "value" }
	},
	resource_appended_to = {
		severity = "PROGRESS_UPDATE",
		arguments = { "resource_id", "new_size" }
	},
	resource_updated = {
		severity = "PROGRESS_UPDATE",
		arguments = { "resource_id", "new_size" }
	},
	resource_eventbus_set = {
		arguments = { "resource_id" }
	},
	resource_downgrade_append = {
		arguments = { "resource_id" }
	},
	resource_downgrade_static = {
		arguments = { "resource_id" }
	},
	transmission_started = {
		arguments = { "uri", "resource_id", "type" }
	},
	transmission_progress = {
		severity = "PROGRESS_UPDATE",
		arguments = { "bytes_transmitted" }
	},
	transmission_aborted = {},
	transmission_finished = {},
	connection_opened = {
		arguments = { "transmission_id", "address" }
	},
	connection_closed = {},
	connection_lost = {
		arguments = { "reason" }
	},
	connection_cancelled = {},
	request_cancelled = {},
	debug_message = {
		severity = "DEBUG",
		arguments = { "unlocalized_message" }
	}
	resource_marked_as_success = {
		arguments = { "resource_id" }
	},
	resource_marked_as_errored = {
		arguments = { "resource_id", "most_significant_error" }
	},
	]]--
	cancel_requested = {
		arguments = { "reason" }
	},
}

function camel_case(str)
	local output = ""
	local prevuscore = true
	for i=1,#str do
		local c = str:sub(i,i)
		if c == "_" then
			prevuscore = true
		else
			if prevuscore then
				output = output..c:upper()
				prevuscore = false
			else
				output = output..c
			end
		end
	end
	return output
end

function write_entry_class_to_file(event_type, severity, arguments)
	f = io.open(event_type..".vala","w+")
	if not f then error("Error opening file "..event_type..".vala in update mode") end
	local function printfunc(str)
		f:write(str.."\n")
	end
	print_log_entry_class(camel_case(event_type), event_type, severity, arguments, printfunc)
	f:flush()
	f:close()
end

function print_log_entry_class(camel_case_name, event_type, severity, arguments, print)
	print("public class Night.Backend.Request.Log.Entry."..camel_case_name.." : Night.Interface.Request.Log.Entry, Object {")
	print("")
	print("\tprivate string _context_id;")
	local contructor_arguments = "";
	local unlocalized_message_arguments = "";
	for _,name in pairs(arguments) do
		print("\tprivate string _"..name..";")
		contructor_arguments = contructor_arguments..", string _"..name 
		unlocalized_message_arguments = unlocalized_message_arguments.."+\"…\"+_"..name..".escape()"
	end
	print("")
	print("\tpublic "..camel_case_name.."(string context_id"..contructor_arguments.."){")
	print("\t\tthis._context_id = context_id;")
	for _,name in pairs(arguments) do
		print("\t\tthis._"..name.." = _"..name..";")
	end
	print("\t}")
	print("")
	print("\tpublic string get_unlocalized_message(){")
	print("\t\treturn \"_!!!_night.request.log."..event_type.."\""..unlocalized_message_arguments..";")
	print("\t}")
	print("")
	print("\tpublic string get_context_id(){")
	print("\t\treturn _context_id;")
	print("\t}")
	print("")
	print("\tpublic string get_event_type(){")
	print("\t\treturn \""..event_type.."\";")
	print("\t}")
	print("")
	print("\tpublic void foreach_argument(Func<string> cb){")
	for _,name in pairs(arguments) do
		print("\t\tcb(\""..name.."\");")
	end
	print("\t}")
	print("")
	print("\tpublic string? get_argument(string key){")
	print("\t\tswitch(key){")
	local return_key = "null"
	local return_value = "null"
	local return_uri = "null"
	local return_resource_id = "null"
	for _,name in pairs(arguments) do
		print("\t\t\tcase \""..name.."\":")
		print("\t\t\t\treturn _"..name..";")
		if name == "key" then return_key = "_"..name end
		if name == "value" then return_value = "_"..name end
		if name == "uri" then return_uri = "_"..name end
		if name == "resource_id" then return_resource_id = "_"..name end
	end
	print("\t\t\tdefault:")
	print("\t\t\t\treturn null;")
	print("\t\t}")
	print("\t}")
	print("")
	print("\tpublic Night.Interface.Request.Log.Severity get_severity(){")
	print("\t\treturn Night.Interface.Request.Log.Severity."..severity..";")
	print("\t}")
	print("")
	print("\t//convenience wrappers for get_argument() (may also be reimplemented to improve performance)")
	print("\tpublic string? get_key(){ return "..return_key.."; }")
	print("\tpublic string? get_value(){ return "..return_value.."; }")
	print("\tpublic string? get_uri(){ return "..return_uri.."; }")
	print("\tpublic string? get_resource_id(){ return "..return_resource_id.."; }")
	print("")
	print("}")
end

for event_type,desc in pairs(entries) do
	write_entry_class_to_file(event_type, desc.severity or "INFO", desc.arguments or {})
end
