local filepath = ...

if not filepath:match("/src/[%a_/.]+%.enum$") then
	error("filepath '"..filepath.."' does not match the required filepath of an enum file")
end

function camel_case(str)
	local output = ""
	local prevuscore = true
	for i=1,#str do
		local c = str:sub(i,i)
		if c == "_" then
			prevuscore = true
		elseif c == "." then
			output = output..c
			prevuscore = true
		else
			if prevuscore then
				output = output..c:upper()
				prevuscore = false
			else
				output = output..c
			end
		end
	end
	return output
end

local enum_file_path = filepath:gsub("%.enum$",".autogen.vala")
local enum_name = camel_case(filepath:match("/([%a_.]+)%.enum$"))
local enum_class_path = "Night."..camel_case(filepath:match(".*/src/([%a_/.]+)%.enum$"):gsub("/","."))

f = io.open(filepath,"r")
if not f then
	error("Error generating enum, file "..filepath.." not found\n")
end

local types = {}
while true do
	local i = f:read()
	if not i then break end
	types[#types+1] = i
end

f:close()

f = io.open(enum_file_path,"w")
if not f then
	error("Error generating enum, file "..enum_file_path.." can not be written to\n")
end

local function print(str)
	f:write(str.."\n")
end

print("public enum "..enum_class_path.." {")
for i = 1,#types do
	if i == #types then
		print("\t"..types[i]:upper()..";")
	else
		print("\t"..types[i]:upper()..",")
	end
end

print("")
print("\tpublic string to_string(){")
print("\t\tswitch(this) {")
for i = 1,#types do
	print("\t\t\tcase "..types[i]:upper()..":")
	print("\t\t\t\treturn \""..types[i].."\";")
end
print("\t\t\tdefault:")
print("\t\t\t\treturn \"unknown\";")
print("\t\t}")
print("\t}")
print("")
print("\tpublic static "..enum_name.."? from_string(string? name){")
print("\t\tif (name == null) { return null; }")
print("\t\tswitch(name) {")
for i = 1,#types do
	print("\t\t\tcase \""..types[i].."\":")
	print("\t\t\t\treturn "..enum_name.."."..types[i]:upper()..";")
end
print("\t\t\tdefault:")
print("\t\t\t\treturn null;")
print("\t\t}")
print("\t}")
print("")	
print("}")

f:flush()
f:close()
