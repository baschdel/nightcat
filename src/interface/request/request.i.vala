public interface Night.Interface.Request.Request : Object {
	
	public abstract void cancel(string reason);
	public abstract string? get_cancellation_reason();
	public abstract bool was_cancelled();
	
	public abstract string get_request_uuid();
	
	//the data it was submitted with	
	public abstract Night.Interface.Request.Action get_action();
	public abstract string get_uri();
	public abstract Night.Interface.ResourceStream.Receiver? get_attached_resource();

	// retrieve context information
	public abstract string get_root_context_uuid();
	public abstract Night.Interface.Request.ContextRole? get_context_role(string context_uuid);
	public abstract string? get_context_name(string context_uuid);
	public abstract string? get_context_uri(string context_uuid);
	public abstract string? get_parent_context_id(string context_uuid);
	public abstract void foreach_direct_child_context_id(Func<string> cb, string context_uuid);
	public abstract Night.Interface.Request.ContextConfiguration? get_context_configuration(string context_uuid);
	public abstract string? get_context_feedback_value(string context_uuid, string key);
	public abstract void foreach_context_feedback(HFunc<string,string> cb, string context_uuid);
	public abstract string? get_context_most_significant_child_id(string context_uuid);
	public abstract string? get_context_most_significant_error(string context_uuid);
	public abstract bool get_context_successful(string context_uuid);
	public abstract void foreach_submitted_resurce_id(Func<string> cb, string? context_uuid, string? uri);
	public abstract bool is_context_open(string context_uuid);
	
	public virtual bool is_root_context_open(){
		return is_context_open(get_root_context_uuid());
	}
	public abstract bool is_any_context_open();
	
	public abstract Night.Interface.Request.Log.Log get_log();
	
	public abstract string? get_resource_uri(string resource_id);
	public abstract string? get_resource_context_id(string resource_id);
	public abstract string? get_resource_most_significant_error(string resource_id);
	public abstract bool get_resource_successful(string resource_id);
	
}
