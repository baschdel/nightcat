public interface Night.Interface.Request.ContextConfiguration : Object {
	
	public abstract string? get_value(string key);
	public abstract void foreach_key(Func<string> cb);
	public abstract void seal(); //makes the configuration read only
	
	public virtual uint64? get_number(string key) {
		string? val = get_value(key);
		if (val == null) { return null; }
		uint64 result;
		if (Night.Util.Intparser.try_parse_unsigned(val ,out result)){
			return result;
		}
		return null;
	}
}
