public interface Night.Interface.Request.Log.Entry : Object {
	
	public abstract string get_unlocalized_message();
	public abstract string get_context_id();
	public abstract string get_event_type();
	public abstract void foreach_argument(Func<string> cb);
	public abstract string? get_argument(string key);
	public abstract Night.Interface.Request.Log.Severity get_severity();
	
	//convenience wrappers for get_argument() (may also be reimplemented to improve performance)
	public virtual string? get_key(){ return get_argument("key"); }
	public virtual string? get_value(){ return get_argument("value"); }
	public virtual string? get_uri(){ return get_argument("uri"); }
	public virtual string? get_resource_id(){ return get_argument("resource_id"); }
}

public enum Night.Interface.Request.Log.Severity {
	ERROR, //An (in the emitting context) unrecoverable error occourred, atrribute is set and context will close
	RECOVERABLE_ERROR, //An error happend but can be recovered, attribute has been set
	WARNING, //Warning attribute set
	INFO, //Context/Resource created/submitted/abandonned
	UPDATE, //Attribute set
	DEBUG, //Debugging only
	PROGRESS_UPDATE; //File transmission progress updated
	
	public string to_string(){
		switch(this){
			case ERROR:
				return "error";
			case RECOVERABLE_ERROR:
				return "recovereable_error";
			case WARNING:
				return "warning";
			case INFO:
				return "info";
			case UPDATE:
				return "update";
			case DEBUG:
				return "debug";
			case PROGRESS_UPDATE:
				return "progress_update";
			default:
				return "unknown";
		}
	}
	
	public Night.Interface.Log.Severity to_log_severity(){
		switch(this){
			case RECOVERABLE_ERROR:
			case WARNING:
				return Night.Interface.Log.Severity.WARNING;
			case INFO:
			case UPDATE:
				return Night.Interface.Log.Severity.INFO;
			case DEBUG:
			case PROGRESS_UPDATE:
				return Night.Interface.Log.Severity.DEBUG;
			case ERROR:
			default:
				return Night.Interface.Log.Severity.ERROR;
		}
	}
}
