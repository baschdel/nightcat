public interface Night.Interface.Request.Log.LogWriter : Night.Interface.Request.Log.Log, Object {

	public abstract void append_log_entry(Night.Interface.Request.Log.Entry entry);
	
}
