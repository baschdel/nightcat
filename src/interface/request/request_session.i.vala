public interface Night.Interface.Request.RequestSession : Object {

	//public abstract Night.Interface.Request.Request make_subrequest(string context_uuid, Night.Interface.Request.Request request, string uri, Night.Interface.Request.Action action, Night.Interface.ResourceStream.Receiver? resource, bool reload = false);
	
	public abstract Night.Interface.Request.Connector get_connector(string parent_context_uuid, Night.Interface.Request.Request request, string address);
	public abstract Night.Interface.Request.NameResolver get_urn_resolver(string urn); //to uris
	public abstract Night.Interface.Request.NameResolver get_authority_resolver(string scheme); //to socket adresses
	public abstract Night.Interface.Request.NameResolver get_host_resolver(string scheme); //to socket address
	//The host resolvers are inteded to be used by authority- and other hostresolvers, the scheme is the protocols to be used or empty (i.e. tls, tcp, tls+tcp)
	
	//if the types do not match the session may return a configuration with an error = type_mismatch key-value pair
	//the session also has to set the uri, address and action keys and must seal the cofiguration.
	public abstract Night.Interface.Request.ContextConfiguration get_actor_configuration(string? parent_context_uuid, Night.Interface.Request.Request request, string name, Night.Interface.Request.ContextRole role, string uri, Night.Interface.Request.Action action, Night.Interface.ResourceStream.Receiver? resource = null);
	public abstract Night.Interface.Request.ContextConfiguration get_transmission_configuration(string? parent_context_uuid, Night.Interface.Request.Request request, string name, Night.Interface.Request.ContextRole role, string uri, string address, Night.Interface.Request.Action action, Night.Interface.ResourceStream.Receiver? resource = null);
	public abstract Night.Interface.Request.ContextConfiguration get_connection_configuration(string parent_context_uuid, Night.Interface.Request.Request request, string name, Night.Interface.Request.ContextRole role, string address);
	public abstract Night.Interface.Request.ContextConfiguration get_host_resolver_configuration(string parent_context_uuid, Night.Interface.Request.Request request, string name, Night.Interface.Request.ContextRole role, string scheme, string hostname, string port);
	public abstract Night.Interface.Request.ContextConfiguration get_authority_resolver_configuration(string parent_context_uuid, Night.Interface.Request.Request request, string name, Night.Interface.Request.ContextRole role, string scheme, string authority);
	public abstract Night.Interface.Request.ContextConfiguration get_urn_resolver_configuration(string parent_context_uuid, Night.Interface.Request.Request request, string name, Night.Interface.Request.ContextRole role, string urn);
}
