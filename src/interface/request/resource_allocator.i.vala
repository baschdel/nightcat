public interface Night.Interface.Request.ResourceAllocator : Object {
	public abstract Night.Interface.ResourceStream.Writer? allocate_resource_for_request(Night.Interface.Request.Request request, string context_uuid, string uri);
}
