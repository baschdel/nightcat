public enum Night.Interface.Request.ContextType {
	ACTOR,
	RESOLVER,
	TRANSMISSION,
	CONNECTOR;
	
	public string to_string(){
		switch(this) {
			case ACTOR:
				return "actor";
			case RESOLVER:
				return "resolver";
			case TRANSMISSION:
				return "transmission";
			case CONNECTOR:
				return "connector";
			default:
				return "unknown";
		}
	}
}
