public interface Night.Interface.Resource.Reader : Night.Interface.ResourceStream.Reader, Object {
	
	public abstract uint64 get_current_position();
	public abstract uint64 get_current_size();
	
	//seeking beyond the start or end of the file will result in the position being clamped
	//the end of the file is defined as the position where no data will be returned by read operations
	public abstract bool is_backward_seekable();
	//trys to seek to to the new position in the file, returns new position
	public abstract uint64 aseek(uint64 abspos);
	
	public signal void updated(bool append_only);
	
	 ///////////////////////////
	// Helper functions
	
	public virtual uint8[]? peek(uint64 maxlen = 10000, Cancellable? cancellable = null){
		if (is_closed()) { return null; }
		var pos = get_current_position();
		var data = read(maxlen, cancellable);
		aseek(pos);
		return data;
	}
	
	//seeks the specified amount of bytes in the resource, returns new position
	public virtual uint64 rseek(int64 relpos){
		if (!is_backward_seekable() && relpos<0 ) { return get_current_position(); }
		if (is_closed()) { return 0; }
		uint64 abspos = get_current_position();
		if (relpos < 0 && relpos+abspos < 0) {
			return aseek(0);
		}
		return aseek(relpos+abspos);
	}
	
	public virtual uint64? get_next_index_of_byte(uint8 byte, uint64 maxlen = 10000){
		if (is_closed()) { return null; }
		uint64 startpos = get_current_position();
		var data = peek(maxlen);
		for (uint64 i = 0; i < data.length; i++){
			if (data[i] == byte) {
				return startpos+i;
			}
		}
		return null;
	}
	
	//peek_when_eod: when set to true the reader will only peek if there is no line end to be within the maximum length
	//was_eod_terminated: is set to true when the line ends without an end of line
	public virtual string? read_line(uint64 maxlen = 10000, Cancellable? cancellable = null, bool peek_when_eod = false, out bool? was_eod_terminated = null){
		was_eod_terminated = false;
		if (is_closed()) { return null; }
		var data = peek(maxlen, cancellable);
		if (data == null) { return null; }
		for (uint64 i = 0; i < data.length; i++){
			if (data[i] == 10 || data[i] == 13) {
				uint64 stringend = i;
				i++;
				if (i < data.length) {
					if (data[i] == 10 || data[i] == 13) {
						i++; 
					}
				}
				rseek((int64) i);
				return ((string) data).slice(0,(long) stringend);
			}
		}
		was_eod_terminated = true;
		if(!peek_when_eod){
			rseek(data.length);
		}
		return ((string) data).slice(0,data.length);
	}
	
	//hit_eod: couldn't find a line neding withing the maximum length
	//if hit_eod and the return value are both false this means that the reder either has closed or is at the end of file
	public virtual bool discard_line(uint64 maxlen = 10000, Cancellable? cancellable = null, out bool? hit_eod = null){
		hit_eod = false;
		if (is_closed()) { return false; }
		var data = peek(maxlen, cancellable);
		if (data == null) { return false; }
		if (data.length == 0) { return false; }
		for (uint64 i = 0; i < data.length; i++){
			if (data[i] == 10 || data[i] == 13) {
				i++;
				if (i < data.length) {
					if (data[i] == 10 || data[i] == 13) {
						i++; 
					}
				}
				rseek((int64) i);
				return true;
			}
		}
		rseek(data.length);
		hit_eod = true;
		return false;
	}
		
}
