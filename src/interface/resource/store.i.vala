public interface Night.Interface.Resource.Store : Object {
	
	public abstract string get_store_uuid();
	
	public abstract bool writable();
	
	public abstract bool has_resorce(string name);
	public abstract Night.Interface.Resource.Resource? retrieve_resource(string name);
	public abstract Night.Interface.Resource.Writer? retrieve_resource_writer(string name);
	public abstract bool create_resource(string name);
	public abstract bool delete_resource(string name);
	
	public abstract bool is_indexable();
	public abstract void foreach_resource(Func<string> cb); //noop if not indexable
	public abstract uint32 count_resources(); //0 could mean unknown if not indexable
	
	public abstract uint64 total_size(); //in bytes 0 means unknown
	public abstract uint64 free_size(); //in bytes 0 means unknown 1 means empty total_size 1 and free_size 1 means unknown but full
}
