public interface Night.Interface.Localization : Object {
	public abstract string localize(string unlocalized_text);
	
	public static string encode_localization_instruction(string rule,string[] args){
		string output = "_!!!_"+rule.escape();
		foreach(string arg in args){
			output += "…"+arg.escape();
		}
		return output;
	}
	
	//the first element in the array is the rule
	public static string[]? decode_localization_instruction(string instruction){
		if (!instruction.has_prefix("_!!!_")) { return null; }
		string[] output = instruction.substring(5).split("…");
		for (int i = 0; i<output.length; i++){
			output[i] = output[i].compress();
		}
		return output;
	}
}

/* the ulocalized text has the following format:
 * _!!!_<localization.rule>[…<escaped string>[…<escaped string>[...]]]
 */
