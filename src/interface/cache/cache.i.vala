public interface Night.Interface.Cache.Cache : Object {
	
	public abstract void foreach_item(Func<string> cb);
	public abstract void foreach_uri(string uri, bool include_expired, Func<string> cb);
	public abstract void foreach_expired(bool include_pinned, Func<string> cb);
	
	public abstract Night.Interface.Cache.StoreReturn? store(Night.Interface.Cache.Info info);
	public abstract Night.Interface.ResourceStream.Receiver? read(string cache_id);
	public abstract Night.Interface.Cache.Info? get_info(string cache_id);
	
	public abstract bool pin(string cache_id);
	public abstract bool remove(string cache_id);
	
}
