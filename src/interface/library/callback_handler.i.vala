public interface Night.Interface.Library.CallbackHandler : Night.Interface.Library.LogListener {
	
	public abstract Night.Interface.ResourceStream.Writer? request_writer_for_downloading(string resource_id, string resource_name);
	
	public abstract Night.Interface.ResourceStream.Reader? request_reader_for_uploading(string resource_id, string resource_name);
	
}
