public interface Night.Interface.Library.Action : Object {
	
	public abstract bool trigger_action(string? argument); //returns true on success
	public abstract bool action_avaiable();
	
}
