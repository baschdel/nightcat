public enum Night.Interface.Library.ContextType {
	LIBRARY,
	CACHE;

	public string to_string(){
		switch(this) {
			case LIBRARY:
				return "library";
			case CACHE:
				return "cache";
			default:
				return "unknown";
		}
	}

	public static ContextType? from_string(string? name){
		if (name == null) { return null; }
		switch(name) {
			case "library":
				return ContextType.LIBRARY;
			case "cache":
				return ContextType.CACHE;
			default:
				return null;
		}
	}

}
