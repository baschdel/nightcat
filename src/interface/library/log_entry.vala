public class Night.Interface.Library.LogEntry : Object {
	
	public string context_id {private set; get;}
	public Night.Interface.Library.LogEntryType log_entry_type {private set; get;}
	public Night.Interface.Library.ContextType context_type {private set; get;}
	public string? reason {protected set; get; default=null;}
	public string? key {private set; get; default=null;}
	public string? val {private set; get; default=null;}
	public string? resource_id {private set; get; default=null;}
	
	public bool trigger_action(string? argument){
		if (action != null) {
			return action.trigger_action(argument);
		} else {
			return false;
		}
	}
	
	public bool action_avaiable(){
		if (action != null) {
			return action.action_avaiable();
		} else {
			return false;
		}
	}
	
	 /////////////////////////////
	// Internals
	
	private Night.Interface.Library.Action? action = null;
	
	 /////////////////////////////
	// private setors
	
	public LogEntry.context_opened(string context_id, ContextType context_type, string context_name, string uri){
		this.log_entry_type = Night.Interface.Library.LogEntryType.CONTEXT_OPENED;
		this.context_id = context_id;
		this.context_type = context_type;
		this.key = context_name;
		this.val = uri;
	}
	
	public LogEntry.action_provided(string context_id, ContextType context_type, Night.Interface.Library.Action action, string action_name, string argument_type){
		this.log_entry_type = Night.Interface.Library.LogEntryType.ACTION_PROVIDED;
		this.context_id = context_id;
		this.context_type = context_type;
		this.action = action;
		this.key = action_name;
		this.val = argument_type;
	}
	
	public LogEntry.action_unavaiable(string context_id, ContextType context_type, string action_name){
		this.log_entry_type = Night.Interface.Library.LogEntryType.ACTION_UNAVAIABLE;
		this.context_id = context_id;
		this.context_type = context_type;
		this.key = action_name;
		this.val = val;
	}
	
	public LogEntry.action_triggered(string context_id, ContextType context_type, string action_name, string? argument){
		this.log_entry_type = Night.Interface.Library.LogEntryType.ACTION_TRIGGERED;
		this.context_id = context_id;
		this.context_type = context_type;
		this.key = action_name;
		this.val = argument;
	}
	
	public LogEntry.feedback(string context_id, ContextType context_type, string key, string? val){
		this.log_entry_type = Night.Interface.Library.LogEntryType.FEEDBACK;
		this.context_id = context_id;
		this.context_type = context_type;
		this.key = key;
		this.val = val;
	}
	
	public LogEntry.resource_writer_requested(string context_id, ContextType context_type, string resource_id, string resource_name){
		this.log_entry_type = Night.Interface.Library.LogEntryType.RESOURCE_WRITER_REQUESTED;
		this.context_id = context_id;
		this.context_type = context_type;
		this.resource_id = resource_id;
		this.key = resource_name;
	}
	
	public LogEntry.resource_writer_delivered(string context_id, ContextType context_type, string resource_id){
		this.log_entry_type = Night.Interface.Library.LogEntryType.RESOURCE_WRITER_DELIVERED;
		this.context_id = context_id;
		this.context_type = context_type;
		this.resource_id = resource_id;
	}
	
	public LogEntry.resource_writer_denyed(string context_id, ContextType context_type, string resource_id){
		this.log_entry_type = Night.Interface.Library.LogEntryType.RESOURCE_WRITER_DENYED;
		this.context_id = context_id;
		this.context_type = context_type;
		this.resource_id = resource_id;
	}
	
	public LogEntry.resource_writer_closed(string context_id, ContextType context_type, string resource_id){
		this.log_entry_type = Night.Interface.Library.LogEntryType.RESOURCE_WRITER_CLOSED;
		this.context_id = context_id;
		this.context_type = context_type;
		this.resource_id = resource_id;
	}
	
	public LogEntry.resource_reader_requested(string context_id, ContextType context_type, string resource_id, string resource_name){
		this.log_entry_type = Night.Interface.Library.LogEntryType.RESOURCE_READER_REQUESTED;
		this.context_id = context_id;
		this.context_type = context_type;
		this.resource_id = resource_id;
		this.key = resource_name;
	}
	
	public LogEntry.resource_reader_delivered(string context_id, ContextType context_type, string resource_id){
		this.log_entry_type = Night.Interface.Library.LogEntryType.RESOURCE_READER_DELIVERED;
		this.context_id = context_id;
		this.context_type = context_type;
		this.resource_id = resource_id;
	}
	
	public LogEntry.resource_reader_denyed(string context_id, ContextType context_type, string resource_id){
		this.log_entry_type = Night.Interface.Library.LogEntryType.RESOURCE_READER_DENYED;
		this.context_id = context_id;
		this.context_type = context_type;
		this.resource_id = resource_id;
	}
	
	public LogEntry.resource_reader_closed(string context_id, ContextType context_type, string resource_id){
		this.log_entry_type = Night.Interface.Library.LogEntryType.RESOURCE_READER_CLOSED;
		this.context_id = context_id;
		this.context_type = context_type;
		this.resource_id = resource_id;
	}
	
	public LogEntry.resource_feedback(string context_id, ContextType context_type, string resource_id, string key, string? val){
		this.log_entry_type = Night.Interface.Library.LogEntryType.RESOURCE_FEEDBACK;
		this.context_id = context_id;
		this.context_type = context_type;
		this.resource_id = resource_id;
		this.key = key;
		this.val = val;
	}
	
	public LogEntry.context_closed_success(string context_id, ContextType context_type){
		this.log_entry_type = Night.Interface.Library.LogEntryType.CONTEXT_CLOSED_SUCCESS;
		this.context_id = context_id;
		this.context_type = context_type;
	}
	
	public LogEntry.context_closed_error(string context_id, ContextType context_type, string key, string? val){
		this.log_entry_type = Night.Interface.Library.LogEntryType.CONTEXT_CLOSED_ERROR;
		this.context_id = context_id;
		this.context_type = context_type;
		this.key = key;
		this.val = val;
	}
	
	 /////////////////////////////
	// Methods
	
	public Night.Interface.Library.LogEntry because(string reason) {
		if (this.reason == null) {
			this.reason = reason;
		}
		return this;
	}
	
}
