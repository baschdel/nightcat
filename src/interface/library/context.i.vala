public delegate bool Night.Interface.Library.ActionCallback(string? argument);

public interface Night.Interface.Library.Context : Object {
	
	public abstract string get_uri();
	public abstract Night.Interface.Library.Verb get_verb();
	public abstract string get_reason();
	
	public abstract bool is_open();
	
	public abstract void submit_feedback(string key, string? val);
	
	public abstract void provide_action(string action_name, owned Night.Interface.Library.ActionCallback callback, string argument_type);
	public abstract void disable_action(string action_name);
	
	public abstract Night.Interface.ResourceStream.Writer? request_writer_for_downloading(string resource_name);
	public abstract Night.Interface.ResourceStream.Reader? request_reader_for_uploading(string resource_name);
	
	public abstract void request_download(Night.Interface.Library.CallbackHandler callback_handler, string uri, string reason, bool reload);
	public abstract void request_upload(Night.Interface.Library.CallbackHandler callback_handler, string uri, string reason);
	public abstract void request_deletion(Night.Interface.Library.CallbackHandler callback_handler, string uri, string reason);
	
	public abstract void close(string? most_significant_error, string? most_significant_error_value);
	
}
