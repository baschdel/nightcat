public interface Night.Interface.ResolverConfiguration.Authority.Query : Object {
	
	public abstract void foreach_alias(string? scheme, Func<Night.Interface.ResolverConfiguration.Alias> cb);
	
	public abstract void foreach_rule(string? scheme, Func<Night.Interface.ResolverConfiguration.Authority.Rule> cb);
	
	/*
	public virtual string export_aliases(){
		string export = "authority_resolver_alias_list";
		this.foreach_alias(null,(alias) => {
			export += "\n"+alias.scheme_template.escape()+"\t"+alias.to_scheme_template.escape();
		});
		return export;
	}
	
	public virtual string export_resolvers(){
		string export = "authority_resolver_resolver_list";
		foreach_resolver(null,(resolver) => {
			string port = "";
			string host_template = "";
			if (resolver.port != null) { port = resolver.port.escape(); }
			if (resolver.host_template != null) { host_template = resolver.host_template.escape(); }
			export += @"\n$(resolver.scheme_template.escape())\t$(resolver.to_scheme_template.escape())\t$(resolver.default_port.escape())\t$port\t$host_template";
		});
		return export;
	}
	
	public virtual string export(){
		var kv = new Night.Util.Kv();
		kv.set_value("type","authority_resolver_configuration");
		kv.set_value("alias_list",this.export_aliases());
		kv.set_value("resolver_list",this.export_resolvers());
		return kv.export();
	}
	*/
	
}
