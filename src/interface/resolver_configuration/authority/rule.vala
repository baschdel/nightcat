public class Night.Interface.ResolverConfiguration.Authority.Rule : Object {
	public string uuid { get; protected set; }
	public string scheme_template { get; protected set; }
	public string? port { get; protected set; }
	public string? host_template { get; protected set; }
	public string to_scheme_template { get; protected set; }
	public string default_port { get; protected set; }
	public Night.Interface.Request.NameCategory category { get; protected set; }
	
	public Rule(string scheme_template, string to_scheme_template, string default_port, string? port = null, string? host_template = null, Night.Interface.Request.NameCategory category = Night.Interface.Request.NameCategory.NEUTRAL){
		this.uuid = GLib.Uuid.string_random();
		this.scheme_template = scheme_template;
		this.to_scheme_template = to_scheme_template;
		this.default_port = default_port;
		this.port = port;
		this.host_template = host_template;
	}
	
	/*
	Rule.construct_import(string configuration){
		this.import(configuration);
	}
	
	public void import(string configuration){
		var kv = new Night.Util.Kv();
		kv.import(configuration);
		kv.set_if_null("scheme_template","");
		kv.set_if_null("to_scheme_template","");
		kv.set_if_null("default_port","");
		this.scheme_template = kv.get_value("scheme_template");
		this.to_scheme_template = kv.get_value("to_scheme_template");
		this.default_port = kv.get_value("default_port");
		this.port = kv.get_value("port");
		this.host_template = kv.get_value("host_template");
	}
	
	public string export(){
		var kv = new Night.Util.Kv();
		kv.set_value("scheme_template",this.scheme_template);
		kv.set_value("to_scheme_template",this.to_scheme_template);
		kv.set_value("default_port",this.default_port);
		if (this.port != null) {
			kv.set_value("port",this.port);
		}
		if (this.host_template != null) {
			kv.set_value("host_template",this.host_template);
		}
		return kv.export();
	}
	*/
	
	public bool matches_scheme(string scheme){
		if (scheme_template.has_suffix("+")) {
			if (scheme+"+" == this.scheme_template) {
				return true;
			} else {
				return scheme.has_prefix(this.scheme_template);
			}
		} else {
			return this.scheme_template == scheme;
		}
	}
	
	public bool matches_host(string host){
		if (this.host_template == null) { return true; }
		if (host_template.has_prefix(".")) {
			if (host.has_suffix(host_template)) {
				return true;
			} else {
				return "."+host == this.host_template;
			}
		} else {
			return this.host_template == host;
		}
	}
	
	public bool matches_port(string? port){
		if (this.port == null) { return true; }
		return this.port == port;
	}
	
	public string get_scheme(string scheme){
		if (to_scheme_template.contains("*")) {
			if (scheme+"+" == this.scheme_template) {
				return to_scheme_template.replace("+*","").replace("*+","").replace("*","");
			} else if (scheme.has_prefix(scheme_template)){
				string suffix = scheme.substring(scheme_template.length);
				return to_scheme_template.replace("*",suffix);
			}
		}
		return to_scheme_template;
	}
	
}
