public class Night.Interface.ResolverConfiguration.Host.Rule : Object {
	public string uuid { get; protected set; }
	public string scheme_template { get; protected set; }
	public string? host_template { get; protected set; }
	public string resolver_group { get; protected set; }
	public Night.Interface.Request.NameCategory? override_category { get; protected set; default=null; }
	
	public Rule(string scheme_template, string resolver_group, string? host_template = null, Night.Interface.Request.NameCategory? override_category = null){
		this.uuid = GLib.Uuid.string_random();
		this.scheme_template = scheme_template;
		this.host_template = host_template;
		this.resolver_group = resolver_group;
		this.override_category = override_category;
	}
	
	public bool matches_scheme(string scheme){
		if (scheme_template.has_suffix("+")) {
			if (scheme+"+" == this.scheme_template) {
				return true;
			} else {
				return scheme.has_prefix(this.scheme_template);
			}
		} else {
			return this.scheme_template == scheme;
		}
	}
	
	public bool matches_host(string host){
		if (this.host_template == null) { return true; }
		if (host_template.has_prefix(".")) {
			if (host.has_suffix(host_template)) {
				return true;
			} else {
				return "."+host == this.host_template;
			}
		} else {
			return this.host_template == host;
		}
	}
}
