public class Night.Interface.ResolverConfiguration.Alias : Object {
	public string scheme_template;
	public string to_scheme_template;
	
	public Alias(string scheme_template, string to_scheme_template){
		this.scheme_template = scheme_template;
		this.to_scheme_template = to_scheme_template;
	}
	
	/*
	Alias.construct_import(string configuration){
		this.import(configuration);
	}
	
	public void import(string configuration){
		var kv = new Night.Util.Kv();
		kv.import(configuration);
		kv.set_if_null("scheme_template","");
		kv.set_if_null("to_scheme_template","");
		this.scheme_template = kv.get_value("scheme_template");
		this.to_scheme_template = kv.get_value("to_scheme_template");
	}
	
	//TODO use same tsv format as above
	
	public string export(){
		var kv = new Night.Util.Kv();
		kv.set_value("scheme_template",this.scheme_template);
		kv.set_value("to_scheme_template",this.to_scheme_template);
		return kv.export();
	}
	*/
	
	public bool matches_scheme(string scheme){
		if (scheme_template.has_suffix("+")) {
			if (scheme+"+" == this.scheme_template) {
				return true;
			} else {
				return scheme.has_prefix(this.scheme_template);
			}
		} else {
			return this.scheme_template == scheme;
		}
	}
	
	public string get_scheme(string scheme){
		if (to_scheme_template.contains("*")) {
			if (scheme+"+" == this.scheme_template) {
				return to_scheme_template.replace("+*","").replace("*+","");
			} else if (scheme.has_prefix(scheme_template)){
				string r = scheme.substring(scheme_template.length);
				return to_scheme_template.replace("*",r);
			}
		}
		return to_scheme_template;
	}
	
}
