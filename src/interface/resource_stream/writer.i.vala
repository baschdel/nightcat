public interface Night.Interface.ResourceStream.Writer : Object {
	public abstract bool close(); //closes the writer
	public abstract bool is_closed();

	public abstract bool set_attribute(string key, string val);
	
	public abstract bool append(uint8[] data, Cancellable? cancellable = null); //returns true if successful
	public abstract bool can_write();
	public signal void awaiting_data();
}
