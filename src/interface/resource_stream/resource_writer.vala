public class Night.Interface.ResourceStream.ResourceWriter : Night.Interface.ResourceStream.Writer, Object {
	
	private Night.Interface.Resource.Writer writer;
	
	public ResourceWriter(Night.Interface.Resource.Writer writer){
		this.writer = writer;
		this.writer.downgrade_append_only();
	}
	
	  ///////////////////////////////////////////////
	 //  Night.Interface.ResourceStream.Receiver  //
	///////////////////////////////////////////////
	
	public bool close(){
		return writer.close();
	}
	
	public bool is_closed(){
		return writer.is_closed();
	}

	public bool set_attribute(string key, string val){
		return writer.set_attribute(key, val);
	}
	
	public bool append(uint8[] data, Cancellable? cancellable = null){
		return writer.append_and_commit(data, cancellable);
	}
	
	public bool can_write(){
		return writer.writable() && writer.can_commit();
	}
	
}
