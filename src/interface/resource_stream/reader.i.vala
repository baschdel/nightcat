public interface Night.Interface.ResourceStream.Reader : Object {
	public abstract void close();
	public abstract bool is_closed();
	
	public abstract uint8[]? read(uint64 maxlen = 10000, Cancellable? cancellable = null);
	public abstract uint8[]? peek(uint64 maxlen = 10000, Cancellable? cancellable = null);
	public abstract bool has_data();
	public signal void data_avaiable();
		
}
