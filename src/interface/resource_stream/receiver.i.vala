public interface Night.Interface.ResourceStream.Receiver : Object {
	public abstract string? get_attribute(string key);
	public signal void attribute_set(string key, string? val);
	
	public abstract Night.Interface.ResourceStream.Reader open_reader();
}
