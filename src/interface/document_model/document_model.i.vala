public interface Night.Interface.DocumentModel.DocumentModel : Object {
	public abstract bool can_write();
	
	public abstract string get_root_node();
	public abstract bool is_node(string node);
	
	public abstract string? get_parent_node(string node);
	public abstract string? get_first_child_node(string node);
	public abstract string? get_last_child_node(string node);
	public abstract string? get_next_node(string node);
	public abstract string? get_previous_node(string node);
	public abstract string? get_node_text(string node);
	public abstract Night.Interface.DocumentModel.NodeType? get_node_type(string node);
	public abstract string[]? get_flags(string node);
	public abstract bool has_flag(string node, string flag);
	
	public abstract string? append_child_node(string node, Night.Interface.DocumentModel.NodeType type, string text);
	public abstract string? prepend_child_node(string node, Night.Interface.DocumentModel.NodeType type, string text);
	public abstract string? insert_node_after(string node, Night.Interface.DocumentModel.NodeType type, string text);
	public abstract string? insert_node_before(string node, Night.Interface.DocumentModel.NodeType type, string text);
	
	public abstract bool remove_node(string node);
	public abstract bool set_node_text(string node, string text);
	public abstract bool replace_node(string node, Night.Interface.DocumentModel.NodeType type, string text); //will keep node_id
	public abstract bool set_node_flag(string node, string flag, bool enabled);
	public abstract bool remove_all_node_flags_with_prefix(string node, string prefix);
	
	//document model is most probbly locked when these are triggered
	public signal void node_created(string node, string? parent);
	public signal void node_removed(string node);
	public signal void node_modified(string node);
}

//* Text - For storing human readable text, that is in the document
//* Block - For grouping elements into paragraphs, sections and whatever else makes sense
//* Uri - For storing an uri from the document for linking and inlining other resources
//* Tag - For marking tags in documents as such
//* Input - For modeling form inputs
//* Action - For defining what buttons do
//* Resource - For inlining external resources, like images, audio etc. may be shown as a link, use only when neccessary and add descriptions

public enum Night.Interface.DocumentModel.NodeType {
	TEXT,
	BLOCK,
	URI,
	TAG,
	INPUT,
	ACTION,
	RESOURCE;
	
	public string to_string(){
		switch(this){
			case TEXT:
				return "text";
			case BLOCK:
				return "block";
			case URI:
				return "uri";
			case TAG:
				return "tag";
			case INPUT:
				return "input";
			case ACTION:
				return "action";
			case RESOURCE:
				return "resource";
			default:
				return "unknown";
		}
	}
}
