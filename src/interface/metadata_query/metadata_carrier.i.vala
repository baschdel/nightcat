public interface Night.Interface.MetadataQuery.MetadataCarrier : Object {
	public abstract string? get_attribute(string key);
	public abstract string get_metadata_type();
	public abstract void foreach_key(Func<string> cb);
}
