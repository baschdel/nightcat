public class Night.Backend.Connector.TcpIp : Night.Interface.Request.Connector, Object {
	
	public uint64 default_timeout = 30;
	
	  /////////////////////////////////////////
	 //  Night.Interface.Request.Connector  //
	/////////////////////////////////////////
	
	public string? request_connection(Night.Interface.Request.ContextInterface context){
		context.initalize_interface("night.connector.tcp_ip", Night.Interface.Request.ContextRole.CONNECTION_SOCKET);
		var config = context.get_my_configuration();
		string? address = config.get_value("address");
		if (address == null) { return "error.no_address"; }
		string? timeout_str = config.get_value("timeout");
		uint64 timeout = default_timeout;
		if (timeout_str != null){
			if (!Night.Util.Intparser.try_parse_unsigned(timeout_str, out timeout)) {
				timeout = default_timeout;
			}
		}
		var parsed_address = Night.Backend.Util.SocketAddress.from_string(address);
		if (parsed_address.sub_address != null) { return "error.invalid_address"; }
		if (!can_handle_address_type(parsed_address.address_type)) { return "error.invalid_address_type"; }
		var arglist = parsed_address.get_arguments();
		if (arglist.length != 2){ return "error.invalid_address"; }
		string ip_address = arglist[1];
		uint64 port;
		context.submit_feedback("ip_address",ip_address);
		context.submit_feedback("port",arglist[0]);
		if (!Night.Util.Intparser.try_parse_unsigned(arglist[0], out port)) { return "error.invalid_address"; }
		if (port > 65535) { return "error.invalid_address"; }
		/////////////////////////////////////////////////////////////////////
		InetSocketAddress? socket_address = new InetSocketAddress.from_string(ip_address, (uint16) port);
		if (socket_address == null) { return "error.invalid_address"; }
		try {
			var client = new SocketClient();
			context.submit_feedback("connection_state","connecting");
			var connection = client.connect(socket_address);
			context.submit_connection(connection, true);
			return null;
		} catch (Error e) {
			context.submit_feedback("connection_state","failed");
			context.submit_feedback("error.exception", e.message);
			return "error.exception";
		}
	}
	
	public bool can_handle_address_type(string address_type){
		switch(address_type){
			case "tcp+ipv4":
			case "tcp+ipv6":
				return true;
			default:
				return false;
		}
	}
	
	public bool can_handle_configuration_key(string address, string key){
		switch(key){
			case "timeout":
				return true;
			default:
				return false;
		}
	}
	
}
