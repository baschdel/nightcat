public class Night.Backend.Connector.Tls : Night.Interface.Request.Connector, Object {
	
	public uint64 default_timeout = 30;
	
	  /////////////////////////////////////////
	 //  Night.Interface.Request.Connector  //
	/////////////////////////////////////////
	
	public string? request_connection(Night.Interface.Request.ContextInterface context){
		context.initalize_interface("night.connector.tls", Night.Interface.Request.ContextRole.CONNECTION_SOCKET);
		var config = context.get_my_configuration();
		string? address = config.get_value("address");
		if (address == null) { return "error.no_address"; }
		var parsed_address = Night.Backend.Util.SocketAddress.from_string(address);
		if (parsed_address.sub_address == null) { return "error.invalid_address.missing_subaddress"; }
		if (!can_handle_address_type(parsed_address.address_type)) { return "error.invalid_address_type"; }
		var arglist = parsed_address.get_arguments();
		if (arglist.length > 2){ return "error.invalid_address.too_many_arguments"; }
		string context_uuid;
		bool connection_established = context.open_connection(parsed_address.sub_address.to_string(), connection_established_callback, out context_uuid);
		context.set_most_significant_child(context_uuid);
		if (connection_established) {
			return null;
		} else {
			return "error.connection_failed";
		}
	}
	
	public bool connection_established_callback(Night.Interface.Request.ContextInterface context, GLib.IOStream connection) throws Error {
		var config = context.get_my_configuration();
		string? address = config.get_value("address");
		if (address == null) {
			context.close_because_of_error("error.no_address");
			return true;
		}
		var parsed_address = Night.Backend.Util.SocketAddress.from_string(address);
		if (parsed_address.sub_address == null) {
			context.close_because_of_error("error.invalid_address.missing_subaddress");
			return true;
		}
		if (!can_handle_address_type(parsed_address.address_type)) {
			context.close_because_of_error("error.invalid_address_type");
			return true;
		}
		var arglist = parsed_address.get_arguments();
		if (arglist.length > 2) {
			context.close_because_of_error("error.invalid_address.too_many_arguments");
			return true;
		}
		string? hostname = config.get_value("tls.sni_hostname");
		string? port = config.get_value("tls.sni_port");
		if (arglist.length >= 1 && hostname == null){
			hostname = arglist[0];
		}
		if (arglist.length >= 2 && port == null){
			port = arglist[1];
		}
		SocketConnectable? server_identity = null;
		uint64? port64 = null;
		Night.Util.Intparser.try_parse_unsigned(port, out port64);
		if (hostname != null && port64 != null){
			if (port64 < 65536) {
				context.submit_feedback("tls.sni.enabled", "true");
				context.submit_feedback("tls.sni.hostname", hostname);
				context.submit_feedback("tls.sni.hostname", port);
				server_identity = server_identity = new NetworkAddress(hostname, (uint16) port64);
			}
		}
		string? client_certificate_pem = config.get_value("tls.client_certificate_pem");
		string? expected_server_certificate_pem = config.get_value("tls.expected_server_certificate_pem");
		var check_settings = new Night.Backend.Connector.Helper.TlsCheckSettings();
		check_settings.import_from_congiguration(config);
		var tls_connection = upgrade_to_tls_connection(connection, client_certificate_pem, expected_server_certificate_pem, check_settings, server_identity);
		tls_connection.submit_feedback(context);
		string? most_significant_error = tls_connection.get_most_significant_error();
		if (most_significant_error != null){
			context.close_because_of_error(most_significant_error);
			return false;
		} else {
			try {
				context.submit_connection(tls_connection.connection, true);
				context.close_because_of_success();
				return false;
			} catch (Error e) {
				context.submit_feedback("error.exception", e.message);
				context.close_because_of_error("error.exception");
				throw e;
			}
		}
	}
	
	public bool can_handle_address_type(string address_type){
		switch(address_type){
			case "tls":
				return true;
			default:
				return false;
		}
	}
	
	public bool can_handle_configuration_key(string address, string key){
		switch(key){
			case "tls.sni_hostname":
			case "tls.check.unexpected_certificate":
			case "tls.check.identity_mismatch":
			case "tls.check.expired":
			case "tls.check.unknown_error":
			case "tls.check.insecure_algorythm":
			case "tls.check.future_activation_date":
			case "tls.check.revoked":
			case "tls.client_certificate_pem":
			case "tls.expected_server_certificate_pem":
				return true;
			default:
				return false;
		}
	}
	
	public static Night.Backend.Connector.Helper.TlsConnection upgrade_to_tls_connection(GLib.IOStream socket, string? client_certificate_pem, string? expected_server_certificate_pem, Night.Backend.Connector.Helper.TlsCheckSettings? check_settings = null, SocketConnectable? server_identity = null){
			try {
				Night.Backend.Connector.Helper.TlsConnection returninfo = new Night.Backend.Connector.Helper.TlsConnection(check_settings);
				returninfo.expected_server_certificate_pem = expected_server_certificate_pem;
				returninfo.connection = TlsClientConnection.@new(socket,server_identity);
				
				if (client_certificate_pem != null){
					var client_certificate = new TlsCertificate.from_pem(client_certificate_pem, client_certificate_pem.length);
					returninfo.connection.set_certificate(client_certificate);
				}
				returninfo.connection.accept_certificate.connect((peer_certificate, tls_errors) => {
					returninfo.tls_errors = tls_errors;
					returninfo.peer_certificate = peer_certificate;
					returninfo.check_server_cerificate();
					return returninfo.may_connect_with_certificate() && returninfo.may_connect_with_error_flags();
				});
				var success = returninfo.connection.handshake();
				if (returninfo.peer_certificate == null) { returninfo.peer_certificate = returninfo.connection.peer_certificate; }
				if (!success){
					return returninfo;
				}
				returninfo.success = true;
				return returninfo;
			} catch (Error e) {
				return new Night.Backend.Connector.Helper.TlsConnection.error(e.message);
			}
		}
	
}

public class Night.Backend.Connector.Helper.TlsCheckSettings {
	public bool unexpected_certificate_critical = false;
	public bool identity_mismatch_critical = true;
	public bool expired_critical = false;
	public bool unknown_error_critical = false;
	public bool insecure_algorythm_critical = false;
	public bool future_activation_date_critical = false;
	public bool revoked_critical = false;
	
	public TlsCheckSettings(){
		//Do nothing
	}
	
	public TlsCheckSettings import_from_congiguration(Night.Interface.Request.ContextConfiguration configuration){
		unexpected_certificate_critical = get_is_critical(configuration,"unexpected_certificate",unexpected_certificate_critical);
		identity_mismatch_critical = get_is_critical(configuration,"identity_mismatch",identity_mismatch_critical);
		expired_critical = get_is_critical(configuration,"expired",expired_critical);
		unknown_error_critical = get_is_critical(configuration,"unknown_error",unknown_error_critical);
		insecure_algorythm_critical = get_is_critical(configuration,"insecure_algorythm",insecure_algorythm_critical);
		future_activation_date_critical = get_is_critical(configuration,"future_activation_date",future_activation_date_critical);
		revoked_critical = get_is_critical(configuration,"revoked",revoked_critical);
		return this;
	}
	
	private static bool get_is_critical(Night.Interface.Request.ContextConfiguration configuration, string check_id, bool default_critical){
		string? val = configuration.get_value("tls.check."+check_id);
		if (val != null){
			return val == "critical";
		} else {
			return default_critical;
		}
	}
}

public class Night.Backend.Connector.Helper.TlsConnection : Object {
	public GLib.TlsConnection? connection = null;
	public TlsCertificateFlags? tls_errors = null;
	public TlsCertificate? peer_certificate = null;
	public string? expected_server_certificate_pem = null;
	public string? error_message = null;
	public bool server_certificate_passed = false;
	public bool success = false;
	
	Night.Backend.Connector.Helper.TlsCheckSettings? check_settings = null;
	
	public TlsConnection(Night.Backend.Connector.Helper.TlsCheckSettings? check_settings){
		this.check_settings = check_settings;
	}
	
	public TlsConnection.error(string error_message){
		this.error_message = error_message;
		check_settings = new TlsCheckSettings();
	}
	
	public void submit_feedback(Night.Interface.Request.ContextInterface context){
		if (tls_errors != null) {
			if ((tls_errors & GLib.TlsCertificateFlags.BAD_IDENTITY) != 0) { context.submit_feedback("warning.tls.certificate.identity_mismatch","true"); }
			if ((tls_errors & GLib.TlsCertificateFlags.EXPIRED) != 0) { context.submit_feedback("warning.tls.certificate.expired","true"); }
			if ((tls_errors & GLib.TlsCertificateFlags.GENERIC_ERROR) != 0) { context.submit_feedback("warning.tls.certificate.unknown_error","true"); }
			if ((tls_errors & GLib.TlsCertificateFlags.INSECURE) != 0) { context.submit_feedback("warning.tls.certificate.insecure_algorythm","true"); }
			if ((tls_errors & GLib.TlsCertificateFlags.NOT_ACTIVATED) != 0) { context.submit_feedback("warning.tls.certificate.future_activation_date","true"); }
			if ((tls_errors & GLib.TlsCertificateFlags.REVOKED) != 0) { context.submit_feedback("warning.tls.certificate.revoked","true"); }
		}
		if (!server_certificate_passed) { context.submit_feedback("warning.tls.certificate.unexpected_certificate","true"); }
		if (error_message != null) { context.submit_feedback("error.exception", error_message); }
		if (error_message == "Unacceptable TLS certificate"){ context.submit_feedback("error.tls.certificate.rejected", "true"); }
		if (peer_certificate != null) {
			context.submit_feedback("tls.server.certificate",peer_certificate.certificate_pem);
		} else {
			context.submit_feedback("warning.server.certificate.missing","true");
		}
	}
	
	public void check_server_cerificate(){
		if (expected_server_certificate_pem == null || peer_certificate == null){
			server_certificate_passed = false;
			return;
		}
		try {
			var expected_certificate = new TlsCertificate.from_pem(expected_server_certificate_pem, expected_server_certificate_pem.length);
			server_certificate_passed = expected_certificate.is_same(peer_certificate);
		} catch (Error e){
			error_message = e.message;
			server_certificate_passed = false;
			return;
		}
	}
	
	public string? get_most_significant_error(){
		check_check_settings();
		if (tls_errors != null) {
			bool identity_mismatch = ((tls_errors & GLib.TlsCertificateFlags.BAD_IDENTITY) != 0);
			bool expired = ((tls_errors & GLib.TlsCertificateFlags.EXPIRED) != 0);
			bool unknown_error = ((tls_errors & GLib.TlsCertificateFlags.GENERIC_ERROR) != 0);
			bool insecure_algorythm = ((tls_errors & GLib.TlsCertificateFlags.INSECURE) != 0);
			bool future_activation_date = ((tls_errors & GLib.TlsCertificateFlags.NOT_ACTIVATED) != 0);
			bool revoked = ((tls_errors & GLib.TlsCertificateFlags.REVOKED) != 0);
			if (check_settings.identity_mismatch_critical && identity_mismatch) { return "error.tls.certificate.identity_mismatch"; }
			if (check_settings.expired_critical && expired) { return "error.tls.certificate.expired"; }
			if (check_settings.unknown_error_critical && unknown_error) { return "error.tls.certificate.unknown_error"; }
			if (check_settings.insecure_algorythm_critical && insecure_algorythm) { return "error.tls.certificate.insecure_algorythm"; }
			if (check_settings.future_activation_date_critical && future_activation_date) { return "error.tls.certificate.future_activation_date"; }
			if (check_settings.revoked_critical && revoked) { return "error.tls.certificate.revoked"; }
		}
		if (check_settings.unexpected_certificate_critical && (!server_certificate_passed)) { return "error.tls.certificate.unexpected_certificate"; }
		if (error_message == "Unacceptable TLS certificate"){ return "error.tls.certificate.rejected"; }
		if (error_message != null) { return "error.exception"; }
		return null;
	}
	
	public bool may_connect_with_certificate(){
		check_check_settings();
		return (!check_settings.unexpected_certificate_critical)  || server_certificate_passed;
	}
	
	public bool may_connect_with_error_flags(){
		check_check_settings();
		bool identity_mismatch = ((tls_errors & GLib.TlsCertificateFlags.BAD_IDENTITY) != 0);
		bool expired = ((tls_errors & GLib.TlsCertificateFlags.EXPIRED) != 0);
		bool unknown_error = ((tls_errors & GLib.TlsCertificateFlags.GENERIC_ERROR) != 0);
		bool insecure_algorythm = ((tls_errors & GLib.TlsCertificateFlags.INSECURE) != 0);
		bool future_activation_date = ((tls_errors & GLib.TlsCertificateFlags.NOT_ACTIVATED) != 0);
		bool revoked = ((tls_errors & GLib.TlsCertificateFlags.REVOKED) != 0);
		bool may_connect = true;
		may_connect = may_connect && !(check_settings.identity_mismatch_critical && identity_mismatch);
		may_connect = may_connect && !(check_settings.expired_critical && expired);
		may_connect = may_connect && !(check_settings.unknown_error_critical && unknown_error);
		may_connect = may_connect && !(check_settings.insecure_algorythm_critical && insecure_algorythm);
		may_connect = may_connect && !(check_settings.future_activation_date_critical && future_activation_date);
		may_connect = may_connect && !(check_settings.revoked_critical && revoked);
		return may_connect;
	}
	
	private void check_check_settings(){
		if (check_settings == null){
			check_settings = new TlsCheckSettings();
		}
	}
}
