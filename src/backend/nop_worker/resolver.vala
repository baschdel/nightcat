public class Night.Backend.NopWorker.ResolverResult : Night.Interface.Request.NameResolverResult, Object {
	
	public static Night.Backend.NopWorker.ResolverResult instance = new ResolverResult();
	
	public string? next(out Night.Interface.Request.NameCategory category = null, out int rating = null){
		category = Night.Interface.Request.NameCategory.NEUTRAL;
		rating = 0;
		return null;
	}
	
	public void close(){}
	
	public bool is_closed(){
		return true;
	}
}

public class Night.Backend.NopWorker.Resolver : Night.Interface.Request.NameResolver, Object {
	
	public static Night.Backend.NopWorker.Resolver instance = new Night.Backend.NopWorker.Resolver();
	
	//if the next() function is called before th online lookup funtion, it will default to an offline lookup.
	//For looking up urns the online mode shouldn't be required
	public Night.Interface.Request.NameResolverResult lookup(Night.Interface.Request.ContextInterface context){
		context.initalize_interface("night.resolver.nop", Night.Interface.Request.ContextRole.AUTHORITY_RESOLVER);
		context.close_because_of_success();
		return Night.Backend.NopWorker.ResolverResult.instance;
	}
	
}
