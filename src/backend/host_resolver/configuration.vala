public class Night.Backend.HostResolver.Configuration : Night.Interface.ResolverConfiguration.Host.Query, Night.Interface.ResolverConfiguration.Host.Write, Object {
	
	private List<Night.Interface.ResolverConfiguration.Alias> aliases = new List<Night.Interface.ResolverConfiguration.Alias>();
	private HashTable<string,Night.Interface.ResolverConfiguration.Host.Resolver> resolvers = new HashTable<string,Night.Interface.ResolverConfiguration.Host.Resolver>(str_hash,str_equal);
	private HashTable<string,Night.Interface.ResolverConfiguration.Host.Rule> rules = new HashTable<string,Night.Interface.ResolverConfiguration.Host.Rule>(str_hash,str_equal);
	
	private bool has_alias(string from_scheme, string to_scheme){
		foreach(var alias in aliases){
			if (from_scheme == alias.scheme_template && alias.to_scheme_template == to_scheme) {
				return true;
			}
		}
		return false;
	}
	
	  //////////////////////////////////////////////////////
	 // Night.Interface.ResolverConfiguration.Host.Write //
	//////////////////////////////////////////////////////
	
	public bool add_alias(string from_scheme, string to_scheme){
		lock(aliases){
			if (!has_alias(from_scheme,to_scheme)){
				aliases.append(new Night.Interface.ResolverConfiguration.Alias(from_scheme, to_scheme));
				return true;
			}
		}
		return false;
	}
	
	public bool remove_alias(string from_scheme, string to_scheme){
		lock(aliases){
			foreach(var alias in aliases){
				if (from_scheme == alias.scheme_template && alias.to_scheme_template == to_scheme) {
					aliases.remove(alias);
					return true;
				}
			}
		}
		return false;
	}
	
	public bool set_resolver(string resolver_name, string group_name, Night.Interface.Request.NameCategory category, Night.Interface.Request.NameResolver resolver){ //only one resolver for each name
		lock(resolvers){
			resolvers.set(resolver_name, new Night.Interface.ResolverConfiguration.Host.Resolver(group_name, resolver_name, category, resolver));
			return true;
		}
	}
	
	public bool remove_resolver(string resolver_name){
		lock(resolvers){
			resolvers.remove(resolver_name);
			return true;
		}
	}
	
	public Night.Interface.Request.NameResolver? get_resolver(string resolver_name){
		lock(resolvers){
			var resolver = resolvers.get(resolver_name);
			if (resolver == null) {
				return null;
			} else {
				return resolver.resolver;
			}
		}
	}
	
	public string? add_rule(string scheme_template, string resolver_group, string? host_template = null, Night.Interface.Request.NameCategory? override_category = null){
		var rule = new  Night.Interface.ResolverConfiguration.Host.Rule(scheme_template, resolver_group, host_template, override_category);
		rules.set(rule.uuid, rule);
		return rule.uuid;
	}
	
	public bool remove_rule(string uuid){
		lock(rules){
			rules.remove(uuid);
			return true;
		}
	}
	
	  //////////////////////////////////////////////////////
	 // Night.Interface.ResolverConfiguration.Host.Query //
	//////////////////////////////////////////////////////
	
	public void foreach_alias(string? scheme, Func<Night.Interface.ResolverConfiguration.Alias> cb){
		foreach(var alias in aliases){
			if (scheme == null) {
				cb(alias);
			} else if (alias.matches_scheme(scheme)) {
				cb(alias);
			}
		}
	}
	
	public void foreach_rule(string? scheme, Func<Night.Interface.ResolverConfiguration.Host.Rule> cb){
		rules.foreach((uuid, rule) => {
			if (scheme == null) {
				cb(rule);
			} else if (rule.matches_scheme(scheme)) {
				cb(rule);
			}
		});
	}
	
	
	public void foreach_resolver(string? group_name, Func<Night.Interface.ResolverConfiguration.Host.Resolver> cb){
		resolvers.foreach((uuid, resolver) => {
			if (group_name == null) {
				cb(resolver);
			} else if (resolver.group_name == group_name) {
				cb(resolver);
			}
		});
	}
	
}
