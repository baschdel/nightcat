public class Night.Backend.AuthorityResolver.Multiplexer : Night.Interface.Request.NameResolver, Object {
	
	Night.Interface.ResolverConfiguration.Authority.Query resolver_configuration;
	
	public Multiplexer(Night.Interface.ResolverConfiguration.Authority.Query configuration){
		this.resolver_configuration = configuration;
	}
	
	
	  ///////////////////////////////////////////
	 //  Night.Interface.Request.NameResolver //
	///////////////////////////////////////////
	
	public Night.Interface.Request.NameResolverResult lookup(Night.Interface.Request.ContextInterface context){
		context.initalize_interface("night.resolver.authority_multiplexing_resolver", Night.Interface.Request.ContextRole.AUTHORITY_RESOLVER);
		var configuration = context.get_my_configuration();
		string? authority = configuration.get_value("authority");
		string? scheme = configuration.get_value("scheme");
		if (authority == null) {
			context.close_because_of_error("error.no_authority");
			return Night.Backend.NopWorker.ResolverResult.instance;
		}
		if (scheme == null) {
			context.close_because_of_error("error.no_scheme");
			return Night.Backend.NopWorker.ResolverResult.instance;
		}
		string? host = null;
		string? port = null;
		var uri = new Night.Util.ParsedUri.blank();
		uri.authority = authority;
		host = uri.host;
		port = uri.port;
		if (host == null) {
			context.close_because_of_error("error.no_host_specified");
			return Night.Backend.NopWorker.ResolverResult.instance;
		}
		List<Night.Interface.ResolverConfiguration.Authority.Rule> rules = new List<Night.Interface.ResolverConfiguration.Authority.Rule>();
		foreach_rule_filtered(scheme, host, port, (rule) => { if (rules.index(rule) < 0) { rules.append(rule); } });
		this.resolver_configuration.foreach_alias(scheme, (alias) => {
			foreach_rule_filtered(alias.get_scheme(scheme), host, port, (rule) => { if (rules.index(rule) < 0) { rules.append(rule); } });
		});
		var multiplexer = new Night.Backend.Util.NameResolverMultiplexer(context);
		rules.sort(compare_rules);
		foreach(var rule in rules){
			multiplexer.append_resolver(new Night.AuthorityResolver.SubResolver(rule.default_port, rule.get_scheme(scheme)), rule.category);
		}
		return multiplexer;
	}
	
	public void foreach_rule_filtered(string scheme, string host, string? port, Func<Night.Interface.ResolverConfiguration.Authority.Rule> cb){
		this.resolver_configuration.foreach_rule(scheme,(rule) => {
			if (rule.matches_scheme(scheme) && rule.matches_host(host) && rule.matches_port(port)) {
				cb(rule);
			}
		});
	}
	
	public static int compare_rules(Night.Interface.ResolverConfiguration.Authority.Rule a, Night.Interface.ResolverConfiguration.Authority.Rule b){
		// a better b = -1 (push to top)
		// a equal b = 0
		// a worse b = 1
		int ascore = get_rule_specifity_score(a);
		int bscore = get_rule_specifity_score(b);
		if (ascore > bscore) { return -1; }
		if (ascore < bscore) { return 1; }
		return 0;
	}
	
	public static int get_rule_specifity_score(Night.Interface.ResolverConfiguration.Authority.Rule rule){
		int s = 0;
		if (!rule.scheme_template.has_suffix("+")) { s += 1; }
		if (rule.port != null) { s += 2; }
		if (rule.host_template != null) { s += 3; }
		return s;
	}
	
}

public class Night.AuthorityResolver.SubResolver : Night.Interface.Request.NameResolver, Object {
	
	private string default_port;
	private string scheme;
	
	public SubResolver(string default_port, string scheme){
		this.default_port = default_port;
		this.scheme = scheme;
	}
	
	  ////////////////////////////////////////////
	 //  Night.Interface.Request.NameResolver  //
	////////////////////////////////////////////
	
	public Night.Interface.Request.NameResolverResult lookup(Night.Interface.Request.ContextInterface context){
		context.initalize_interface("night.resolver.authority_host_bridge", Night.Interface.Request.ContextRole.AUTHORITY_RESOLVER);
		var configuration = context.get_my_configuration();
		string? authority = configuration.get_value("authority");
		if (authority == null) {
			context.close_because_of_error("error.no_authority");
			return Night.Backend.NopWorker.ResolverResult.instance;
		}
		string? host = null;
		string? port = null;
		var uri = new Night.Util.ParsedUri.blank();
		uri.authority = authority;
		host = uri.host;
		port = uri.port;
		if (host == null) {
			context.close_because_of_error("error.no_host_specified");
			return Night.Backend.NopWorker.ResolverResult.instance;
		}
		if (port == null) {
			port = this.default_port;
		}
		return new Night.AuthorityResolver.SubResolverResult(context, context.resolve_host(this.scheme, host, port));
	}
	
}

public class Night.AuthorityResolver.SubResolverResult : Night.Interface.Request.NameResolverResult, Object {
	
	private Night.Interface.Request.ContextInterface context;
	private Night.Interface.Request.NameResolverResult result;
	
	public SubResolverResult(Night.Interface.Request.ContextInterface context, Night.Interface.Request.NameResolverResult result){
		this.context = context;
		this.result = result;
	}
	
	  //////////////////////////////////////////////////
	 //  Night.Interface.Request.NameResolverResult  //
	//////////////////////////////////////////////////
	
	public string? next(out Night.Interface.Request.NameCategory category = null, out int rating = null){
		string? res = result.next(out category, out rating);
		if (res == null) { context.close_because_of_success(); }
		return res;
	}
	
	public void close(){
		result.close();
		context.close_because_of_success();
	}
	
	public bool is_closed(){
		if (result.is_closed()) {
			if (context.is_open()) {
				context.close_because_of_success();
			}
			return true;
		} else {
			return false;
		}
	}
	
	public Night.Interface.Request.NameCategory? peek_next_category(){
		return result.peek_next_category();
	}
	
}
