public class Night.Backend.AuthorityResolver.Configuration : Night.Interface.ResolverConfiguration.Authority.Query, Night.Interface.ResolverConfiguration.Authority.Write, Object {
	
	private List<Night.Interface.ResolverConfiguration.Alias> aliases = new List<Night.Interface.ResolverConfiguration.Alias>();
	private HashTable<string,Night.Interface.ResolverConfiguration.Authority.Rule> rules = new HashTable<string,Night.Interface.ResolverConfiguration.Authority.Rule>(str_hash,str_equal);
	
	private bool has_alias(string from_scheme, string to_scheme){
		foreach(var alias in aliases){
			if (from_scheme == alias.scheme_template && alias.to_scheme_template == to_scheme) {
				return true;
			}
		}
		return false;
	}
	
	  ///////////////////////////////////////////////////////////
	 // Night.Interface.ResolverConfiguration.Authority.Write //
	///////////////////////////////////////////////////////////
	
	public bool add_alias(string from_scheme, string to_scheme){
		lock(aliases){
			if (!has_alias(from_scheme,to_scheme)){
				aliases.append(new Night.Interface.ResolverConfiguration.Alias(from_scheme, to_scheme));
				return true;
			}
		}
		return false;
	}
	
	public bool remove_alias(string from_scheme, string to_scheme){
		lock(aliases){
			foreach(var alias in aliases){
				if (from_scheme == alias.scheme_template && alias.to_scheme_template == to_scheme) {
					aliases.remove(alias);
					return true;
				}
			}
		}
		return false;
	}
	
	public string? add_rule(string scheme_template, string to_scheme_template, string default_port, string? port = null, string? host_template = null, Night.Interface.Request.NameCategory category = Night.Interface.Request.NameCategory.NEUTRAL){
		var rule = new  Night.Interface.ResolverConfiguration.Authority.Rule(scheme_template, to_scheme_template, default_port, port, host_template, category);
		rules.set(rule.uuid, rule);
		return rule.uuid;
	}
	
	public bool remove_rule(string uuid){
		lock(rules){
			rules.remove(uuid);
			return true;
		}
	}
	
	  ///////////////////////////////////////////////////////////
	 // Night.Interface.ResolverConfiguration.Authority.Query //
	///////////////////////////////////////////////////////////
	
	public void foreach_alias(string? scheme, Func<Night.Interface.ResolverConfiguration.Alias> cb){
		foreach(var alias in aliases){
			if (scheme == null) {
				cb(alias);
			} else if (alias.matches_scheme(scheme)) {
				cb(alias);
			}
		}
	}
	
	public void foreach_rule(string? scheme, Func<Night.Interface.ResolverConfiguration.Authority.Rule> cb){
		rules.foreach((uuid, rule) => {
			if (scheme == null) {
				cb(rule);
			} else if (rule.matches_scheme(scheme)) {
				cb(rule);
			}
		});
	}
	
}
