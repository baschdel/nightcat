public class Night.Backend.Util.NameResolverMultiplexer : Night.Interface.Request.NameResolverResult, Object {
	private List<NameResolverMultiplexerInput> inputs = new List<NameResolverMultiplexerInput>();
	private Night.Interface.Request.ContextInterface context;
	private NameResolverMultiplexerInput? result_cache = null;
	
	public NameResolverMultiplexer(Night.Interface.Request.ContextInterface context){
		this.context = context;
	}
	
	public bool contains_resolver(Night.Interface.Request.NameResolver resolver){
		foreach(var input in inputs){
			if (input.uses_resolver(resolver)) {
				return true;
			}
		}
		return false;
	}
	
	public void append_resolver(Night.Interface.Request.NameResolver resolver, Night.Interface.Request.NameCategory resolver_category = Night.Interface.Request.NameCategory.NEUTRAL){
		if (!this.contains_resolver(resolver)) {
			inputs.append(new NameResolverMultiplexerInput(resolver, resolver_category));
		}
	}
	
	private NameResolverMultiplexerInput? next_result(){
		if (this.result_cache != null) { return this.result_cache; }
		NameResolverMultiplexerInput? res = null;
		foreach (var input in this.inputs) {
			if (!input.is_closed()) {
				if (res == null) {
					res = input;
				} else {
					if (is_x_better(input,res)) {
						res = input;
					}
				}
			}
		}
		this.result_cache = res;
		return res;
	}
	
	private bool is_x_better(NameResolverMultiplexerInput x, NameResolverMultiplexerInput y){
		if (x.is_closed()) { return false; }
		if (y.is_closed()) { return true; }
		Night.Interface.Request.NameCategory? x_category = x.peek_next_category();
		Night.Interface.Request.NameCategory? y_category = y.peek_next_category();
		if (x_category == null && x.looked_up) { x_category = x.result_category; }
		if (y_category == null && y.looked_up) { y_category = y.result_category; }
		if (x_category == null) { x_category = x.resolver_category; }
		if (y_category == null) { y_category = y.resolver_category; }
		if (x_category == y_category) {
			if (x.looked_up == y.looked_up) {
				return x.result_rating > y.result_rating;
			} else {
				return x.looked_up;
			}
		} else {
			return x_category > y_category;
		}
	}
	
	  //////////////////////////////////////////////////
	 //  Night.Interface.Request.NameResolverResult  //
	//////////////////////////////////////////////////
	
	public string? next(out Night.Interface.Request.NameCategory category = null, out int rating = null){
		category = Night.Interface.Request.NameCategory.NEUTRAL;
		rating = 0;
		var input = next_result();
		if (input == null) {
			close();
			return null;
		}
		if (!input.looked_up) {
			var ccontext = context.create_resolver_subinterface();
			if (ccontext == null) {
				// This should only be the case if the multiplexer context isn't a resolver context.
				return null;
			}
			input.lookup(ccontext);
		}
		category = input.result_category;
		rating = input.result_rating;
		string? result = input.next_result;
		input.next();
		result_cache = null;
		return result;
	}
	
	public void close(){
		foreach(var input in inputs) {
			input.close();
		}
		context.close_because_of_success();
	}
	
	public bool is_closed(){
		foreach(var input in inputs) {
			if (!input.is_closed()) {
				return false;
			}
		}
		return true;
	}
	
	public Night.Interface.Request.NameCategory? peek_next_category(){
		var next_result = this.next_result();
		if (next_result == null) { return null; }
		return next_result.peek_next_category();
	}
	
}

public class Night.Backend.Util.NameResolverMultiplexerInput : Object {
	private Night.Interface.Request.NameResolverResult? result = null;
	private Night.Interface.Request.NameResolver resolver;
	public Night.Interface.Request.NameCategory resolver_category { get; protected set; }
	public bool looked_up { get; protected set; default=false; }
	
	public string? next_result { get; protected set; default=null; }
	public Night.Interface.Request.NameCategory result_category { get; protected set; default=Night.Interface.Request.NameCategory.NEUTRAL; }
	public int result_rating { get; protected set; default=0; }
	
	public NameResolverMultiplexerInput(Night.Interface.Request.NameResolver resolver, Night.Interface.Request.NameCategory resolver_category = Night.Interface.Request.NameCategory.NEUTRAL){
		this.resolver = resolver;
		this.resolver_category = resolver_category;
	}
	
	public bool uses_resolver(Night.Interface.Request.NameResolver resolver){
		return this.resolver == resolver;
	}
	
	public void lookup(Night.Interface.Request.ContextInterface context){
		lock (this.looked_up) {
			if (this.looked_up) { return; }
			this.result = resolver.lookup(context);
			this.looked_up = true;
			next();
		}
	}
	
	public void next(){
		Night.Interface.Request.NameCategory category = Night.Interface.Request.NameCategory.NEUTRAL;
		int rating = 0;
		next_result = result.next(out category, out rating);
		this.result_category = result_category;
		this.result_rating = rating;
	}
	
	public void close(){
		if (result != null) {
			result.close();
			next_result = null;
		}
	}
	
	public bool is_closed(){
		if (this.result == null) { return this.looked_up; }
		return result.is_closed();
	}
	
	public Night.Interface.Request.NameCategory? peek_next_category(){
		if (this.result == null) { return this.resolver_category; }
		return result.peek_next_category();
	}
	
}
