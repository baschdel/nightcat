public class Night.Backend.Session.SimpleSession.FetcherManager : Object {
	
	private HashTable<string,Night.Interface.Request.Fetcher> fetchers = new HashTable<string,Night.Interface.Request.Fetcher>(str_hash,str_equal);
	private Night.Interface.Request.Fetcher default_fetcher;
	
	public FetcherManager(Night.Interface.Request.Fetcher default_fetcher){
		this.default_fetcher = default_fetcher;
	}
	
	public Night.Interface.Request.Fetcher get_fetcher_for(string scheme){
		string _scheme = scheme;
		while (true){
			var fetcher = fetchers.get(_scheme);
			if (fetcher != null) {
				return fetcher;
			}
			int i = _scheme.last_index_of_char('+');
			if (i > 0) {
				_scheme = _scheme.substring(0,i);
			} else {
				break;
			}
		}
		return default_fetcher;
	}
	
	public void set_fetcher(Night.Interface.Request.Fetcher? fetcher, string? scheme = null){
		if (scheme == null) {
			if (fetcher != null) {
				default_fetcher = fetcher;
			}
		} else {
			fetchers.set(scheme,fetcher);
		}
	}
	
}
