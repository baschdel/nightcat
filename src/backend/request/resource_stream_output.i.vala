public class Might.Backend.Request.ResourceStreamOutput : Night.Interface.Request.ResourceStreamOutput, Object {
	
	public string output_id;
	public Night.Backend.Request.ContextInterface context_interface;
	public Night.Interface.ResourceStream.Writer resource_stream_writer;
	
	public ResourceStreamOutput(Night.Interface.ResourceStream.Writer resource_stream_writer, Night.Backend.Request.ContextInterface context_interface, string output_id){
		this.resource_stream_writer = resource_stream_writer;
		this.context_interface = context_interface;
		this.output_id = output_id;
	}
	
	public Night.Interface.ResourceStream.Writer get_writer(){
		return resource_stream_writer;
	}
	
	public void mark_as_success(){
		context_interface.mark_resource_as_success(output_id);
	}
	
	public void mark_as_errored(string most_significant_error){
		context_interface.mark_resource_as_errored(output_id, most_significant_error);
	}
	
}
