public class Night.Backend.Request.Log.Logger : Night.Interface.Request.Log.Log, Object {
	
	private List<Night.Interface.Request.Log.Entry> entries = new List<Night.Interface.Request.Log.Entry>();
	public bool record_debug_messages = false;
	
	  ///////////////////////////////////////
	 //  Night.Interface.Request.Log.Log  //
	///////////////////////////////////////
	
	public Night.Interface.Request.Log.Entry? get_entry(uint32 n){
		lock(entries){
			return entries.nth_data(n);
		}
	}
	
	public uint32 get_n_entrys(){
		lock(entries){
			return entries.length();
		}
	}
	
	public bool records_debug_messages(){
		lock(entries){
			return record_debug_messages;
		}
	}
	
	public void foreach(Func<Night.Interface.Request.Log.Entry> cb){ //iterates over all entrys in chronological order
		lock(entries){
			entries.foreach(cb);
		}
	}
	
	  /////////////////////////////////////////////
	 //  Night.Interface.Request.Log.LogWriter  //
	/////////////////////////////////////////////
	
	public void append_log_entry(Night.Interface.Request.Log.Entry entry){
		switch(entry.get_severity()){
			case Night.Interface.Request.Log.Severity.DEBUG:
				if (!record_debug_messages){ return; }
				break;
			case Night.Interface.Request.Log.Severity.ERROR:
			case Night.Interface.Request.Log.Severity.RECOVERABLE_ERROR:
			case Night.Interface.Request.Log.Severity.WARNING:
			case Night.Interface.Request.Log.Severity.INFO:
			case Night.Interface.Request.Log.Severity.UPDATE:
				break;
			case Night.Interface.Request.Log.Severity.PROGRESS_UPDATE:
			default:
				return;
		}
		lock(entries){
			entries.append(entry);					
			entry_added(entries.length()-1,entry);
		}
	}
	
}
