public class Night.Backend.Request.Log.Entry.ContextCreated : Night.Interface.Request.Log.Entry, Object {

	private string _context_id;
	private string? _parent_context_id;
	private Night.Interface.Request.ContextRole role;

	public ContextCreated(string context_id, string? _parent_context_id, Night.Interface.Request.ContextRole role){
		this._context_id = context_id;
		this._parent_context_id = _parent_context_id;
		this.role = role;
	}

	public string get_unlocalized_message(){
		if (_parent_context_id != null) {
			return "_!!!_night.request.log.context_created…"+_parent_context_id.escape()+"…"+role.to_string().escape();
		} else {
			return "_!!!_night.request.log.root_context_created…"+role.to_string().escape();
		}
	}

	public string get_context_id(){
		return _context_id;
	}

	public string get_event_type(){
		return "context_created";
	}

	public void foreach_argument(Func<string> cb){
		if (_parent_context_id != null) { cb("parent_context_id"); }
		cb("context_type");
		cb("context_role");
	}

	public string? get_argument(string key){
		switch(key){
			case "parent_context_id":
				return _parent_context_id;
			case "context_type":
				return role.get_context_type().to_string();
			case "context_role":
				return role.to_string();
			default:
				return null;
		}
	}

	public Night.Interface.Request.Log.Severity get_severity(){
		return Night.Interface.Request.Log.Severity.INFO;
	}

	//convenience wrappers for get_argument() (may also be reimplemented to improve performance)
	public string? get_key(){ return null; }
	public string? get_value(){ return null; }
	public string? get_uri(){ return null; }
	public string? get_resource_id(){ return null; }

}
