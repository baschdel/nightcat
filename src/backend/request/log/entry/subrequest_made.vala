public class Night.Backend.Request.Log.Entry.SubrequestMade : Night.Interface.Request.Log.Entry, Object {
	
	private string _context_id;
	private string _subrequest_uuid;
	private string _uri;
	private string _action;
	private string? _resource_id;
	
	public SubrequestMade(string context_id, string _subrequest_uuid, string _uri, string _action, string? _resource_id = null){
		this._context_id = context_id;
		this._subrequest_uuid = _subrequest_uuid;
		this._uri = _uri;
		this._action = _action;
		this._resource_id = _resource_id;
	}
	
	public string get_unlocalized_message(){
		if (_resource_id != null) {
			return "_!!!_night.request.log.subrequest_made.with_resource…"+_subrequest_uuid.escape()+"…"+_uri.escape()+"…"+_action.escape()+"…"+_resource_id.escape();
		} else {
			return "_!!!_night.request.log.subrequest_made…"+_subrequest_uuid.escape()+"…"+_uri.escape()+"…"+_action.escape();
		}
	}
	
	public string get_context_id(){
		return _context_id;
	}
	
	public string get_event_type(){
		return "subrequest_made";
	}
	
	public void foreach_argument(Func<string> cb){
		cb("subrequest_uuid");
		cb("uri");
		cb("action");
		if (_resource_id != null){ cb("resource_id"); }
	}
	
	public string? get_argument(string key){
		switch(key){
			case "subrequest_uuid":
				return _subrequest_uuid;
			case "uri":
				return _uri;
			case "action":
				return _action;
			case "resource_id":
				return _resource_id;
			default:
				return null;
		}
	}
	
	public Night.Interface.Request.Log.Severity get_severity(){
		return Night.Interface.Request.Log.Severity.INFO;
	}
	
	//convenience wrappers for get_argument() (may also be reimplemented to improve performance)
	public string? get_key(){ return null; }
	public string? get_value(){ return null; }
	public string? get_uri(){ return _uri; }
	public string? get_resource_id(){ return _resource_id; }
	
}
