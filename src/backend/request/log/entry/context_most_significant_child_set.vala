public class Night.Backend.Request.Log.Entry.ContextMostSignificantChildSet : Night.Interface.Request.Log.Entry, Object {

	private string _context_id;
	private string _most_significant_child;

	public ContextMostSignificantChildSet(string context_id, string _most_significant_child){
		this._context_id = context_id;
		this._most_significant_child = _most_significant_child;
	}

	public string get_unlocalized_message(){
		return "_!!!_night.request.log.context_most_significant_child_set"+"…"+_most_significant_child.escape();
	}

	public string get_context_id(){
		return _context_id;
	}

	public string get_event_type(){
		return "context_most_significant_child_set";
	}

	public void foreach_argument(Func<string> cb){
		cb("most_significant_child");
	}

	public string? get_argument(string key){
		switch(key){
			case "most_significant_child":
				return _most_significant_child;
			default:
				return null;
		}
	}

	public Night.Interface.Request.Log.Severity get_severity(){
		return Night.Interface.Request.Log.Severity.INFO;
	}

	//convenience wrappers for get_argument() (may also be reimplemented to improve performance)
	public string? get_key(){ return null; }
	public string? get_value(){ return null; }
	public string? get_uri(){ return null; }
	public string? get_resource_id(){ return null; }

}
