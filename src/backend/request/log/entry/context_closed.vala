public class Night.Backend.Request.Log.Entry.ContextClosed : Night.Interface.Request.Log.Entry, Object {

	private string _context_id;
	private string? _most_significant_error;

	public ContextClosed(string context_id, string? _most_significant_error){
		this._context_id = context_id;
		this._most_significant_error = _most_significant_error;
	}

	public string get_unlocalized_message(){
		if (_most_significant_error != null) {
			return "_!!!_night.request.log.context_closed_error"+"…"+_most_significant_error.escape();
		} else {
			return "_!!!_night.request.log.context_closed_success";
		}
	}

	public string get_context_id(){
		return _context_id;
	}

	public string get_event_type(){
		return "context_closed";
	}

	public void foreach_argument(Func<string> cb){
		if (_most_significant_error != null){
			cb("most_significant_error");
		}
	}

	public string? get_argument(string key){
		switch(key){
			case "most_significant_error":
				return _most_significant_error;
			default:
				return null;
		}
	}

	public Night.Interface.Request.Log.Severity get_severity(){
		if (_most_significant_error == null) {
			return Night.Interface.Request.Log.Severity.INFO;
		} else {
			return Night.Interface.Request.Log.Severity.ERROR;
		}
	}

	//convenience wrappers for get_argument() (may also be reimplemented to improve performance)
	public string? get_key(){ return null; }
	public string? get_value(){ return null; }
	public string? get_uri(){ return null; }
	public string? get_resource_id(){ return null; }

}
