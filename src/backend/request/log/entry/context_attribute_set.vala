public class Night.Backend.Request.Log.Entry.ContextAttributeSet : Night.Interface.Request.Log.Entry, Object {

	private string _context_id;
	private string _key;
	private string _value;
	private Night.Interface.Request.Log.Severity _severity;

	public ContextAttributeSet(string context_id, string _key, string _value){
		this._context_id = context_id;
		this._key = _key;
		this._value = _value;
		if (_key.has_prefix("error.")) {
			_severity = Night.Interface.Request.Log.Severity.ERROR;
		} else if (_key.has_prefix("warning.")) {
			_severity = Night.Interface.Request.Log.Severity.WARNING;
		} else {
			_severity = Night.Interface.Request.Log.Severity.UPDATE;
		}
	}

	public string get_unlocalized_message(){
		return "_!!!_night.request.log.context_attribute_set"+"…"+_key.escape()+"…"+_value.escape();
	}

	public string get_context_id(){
		return _context_id;
	}

	public string get_event_type(){
		return "context_attribute_set";
	}

	public void foreach_argument(Func<string> cb){
		cb("key");
		cb("value");
	}

	public string? get_argument(string key){
		switch(key){
			case "key":
				return _key;
			case "value":
				return _value;
			default:
				return null;
		}
	}

	public Night.Interface.Request.Log.Severity get_severity(){
		return _severity;
	}

	//convenience wrappers for get_argument() (may also be reimplemented to improve performance)
	public string? get_key(){ return _key; }
	public string? get_value(){ return _value; }
	public string? get_uri(){ return null; }
	public string? get_resource_id(){ return null; }

}
