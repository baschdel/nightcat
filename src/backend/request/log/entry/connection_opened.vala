public class Night.Backend.Request.Log.Entry.ConnectionOpened : Night.Interface.Request.Log.Entry, Object {

	private string _context_id;
	private string _transmission_id;
	private string _address;

	public ConnectionOpened(string context_id, string _transmission_id, string _address){
		this._context_id = context_id;
		this._transmission_id = _transmission_id;
		this._address = _address;
	}

	public string get_unlocalized_message(){
		return "_!!!_night.request.log.connection_opened"+"…"+_transmission_id.escape()+"…"+_address.escape();
	}

	public string get_context_id(){
		return _context_id;
	}

	public string get_event_type(){
		return "connection_opened";
	}

	public void foreach_argument(Func<string> cb){
		cb("transmission_id");
		cb("address");
	}

	public string? get_argument(string key){
		switch(key){
			case "transmission_id":
				return _transmission_id;
			case "address":
				return _address;
			default:
				return null;
		}
	}

	public Night.Interface.Request.Log.Severity get_severity(){
		return Night.Interface.Request.Log.Severity.INFO;
	}

	//convenience wrappers for get_argument() (may also be reimplemented to improve performance)
	public string? get_key(){ return null; }
	public string? get_value(){ return null; }
	public string? get_uri(){ return null; }
	public string? get_resource_id(){ return null; }

}
