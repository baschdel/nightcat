public class Night.Backend.Request.Log.Entry.ResourceAttributeSet : Night.Interface.Request.Log.Entry, Object {

	private string _context_id;
	private string _resource_id;
	private string _key;
	private string _value;
	private Night.Interface.Request.Log.Severity _severity;

	public ResourceAttributeSet(string context_id, string _resource_id, string _key, string _value){
		this._context_id = context_id;
		this._resource_id = _resource_id;
		this._key = _key;
		this._value = _value;
		if (_key.has_prefix("error.")) {
			_severity = Night.Interface.Request.Log.Severity.ERROR;
		} else if (_key.has_prefix("warning.")) {
			_severity = Night.Interface.Request.Log.Severity.WARNING;
		} else {
			_severity = Night.Interface.Request.Log.Severity.UPDATE;
		}
	}

	public string get_unlocalized_message(){
		return "_!!!_night.request.log.resource_attribute_set"+"…"+_resource_id.escape()+"…"+_key.escape()+"…"+_value.escape();
	}

	public string get_context_id(){
		return _context_id;
	}

	public string get_event_type(){
		return "resource_attribute_set";
	}

	public void foreach_argument(Func<string> cb){
		cb("resource_id");
		cb("key");
		cb("value");
	}

	public string? get_argument(string key){
		switch(key){
			case "resource_id":
				return _resource_id;
			case "key":
				return _key;
			case "value":
				return _value;
			default:
				return null;
		}
	}

	public Night.Interface.Request.Log.Severity get_severity(){
		return _severity;
	}

	//convenience wrappers for get_argument() (may also be reimplemented to improve performance)
	public string? get_key(){ return _key; }
	public string? get_value(){ return _value; }
	public string? get_uri(){ return null; }
	public string? get_resource_id(){ return _resource_id; }

}
