public class Night.Backend.Request.Log.Entry.TransmissionProgress : Night.Interface.Request.Log.Entry, Object {

	private string _context_id;
	private string _bytes_transmitted;

	public TransmissionProgress(string context_id, string _bytes_transmitted){
		this._context_id = context_id;
		this._bytes_transmitted = _bytes_transmitted;
	}

	public string get_unlocalized_message(){
		return "_!!!_night.request.log.transmission_progress"+"…"+_bytes_transmitted.escape();
	}

	public string get_context_id(){
		return _context_id;
	}

	public string get_event_type(){
		return "transmission_progress";
	}

	public void foreach_argument(Func<string> cb){
		cb("bytes_transmitted");
	}

	public string? get_argument(string key){
		switch(key){
			case "bytes_transmitted":
				return _bytes_transmitted;
			default:
				return null;
		}
	}

	public Night.Interface.Request.Log.Severity get_severity(){
		return Night.Interface.Request.Log.Severity.PROGRESS_UPDATE;
	}

	//convenience wrappers for get_argument() (may also be reimplemented to improve performance)
	public string? get_key(){ return null; }
	public string? get_value(){ return null; }
	public string? get_uri(){ return null; }
	public string? get_resource_id(){ return null; }

}
