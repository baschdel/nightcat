public class Night.Backend.Request.Helper.LoggedWriter : Night.Interface.Resource.Writer, Object {

	public Night.Interface.Resource.Writer resource_writer { get; protected set; }
	public string resource_id { get; protected set; }
	public string context_id { get; protected set; }
	
	public signal void log_event(Night.Interface.Request.Log.Entry entry);
	
	public LoggedWriter(Night.Interface.Resource.Writer resource_writer, string context_id){
		this.resource_writer = resource_writer;
		this.resource_id = resource_writer.get_resource().get_resource_uid();
		this.context_id = context_id;
	}

	public bool clear(Cancellable? cancellable = null){
		return resource_writer.clear();
	}
	
	public bool append(uint8[] data, Cancellable? cancellable = null){
		return resource_writer.append(data, cancellable);
	}
	
	public bool commit(Cancellable? cancellable = null){
		return resource_writer.commit(cancellable);
	}
	
	public void reset(){
		resource_writer.reset();
	}
	
	public bool append_and_commit(uint8[] data, Cancellable? cancellable = null){
		return resource_writer.append_and_commit(data, cancellable);
	}
	
	public bool delete_resource(){
		if (resource_writer.delete_resource()) {
			log_event(new Night.Backend.Request.Log.Entry.ResourceDropped(context_id, resource_id));
			return true;
		} else {
			return false;
		}
	}
	
	public bool writable(){
		return resource_writer.writable();
	}
	
	public bool updatable(){
		return resource_writer.updatable();
	}
	
	public bool can_commit(){
		return resource_writer.can_commit();
	}
	
	public bool is_closed(){
		return resource_writer.is_closed();
	}
	
	public bool close(){
		return resource_writer.close();
	}
	
	public bool downgrade_append_only(){
		if (resource_writer.downgrade_append_only()) {
			log_event(new Night.Backend.Request.Log.Entry.ResourceDowngradeAppend(context_id, resource_id));
			return true;
		} else {
			return false;
		}
	}
	
	public bool downgrade_read_only(){
		if (resource_writer.downgrade_read_only()) {
			log_event(new Night.Backend.Request.Log.Entry.ResourceDowngradeStatic(context_id, resource_id));
			return true;
		} else {
			return false;
		}
	}
	
	public bool set_eventbus(Night.Util.Eventbus eventbus){
		if (resource_writer.set_eventbus(eventbus)) {
			log_event(new Night.Backend.Request.Log.Entry.ResourceEventbusSet(context_id, resource_id));
			return true;
		} else {
			return false;
		}
	}
	
	public bool set_attribute(string key, string val){
		if (resource_writer.set_attribute(key, val)) {
			log_event(new Night.Backend.Request.Log.Entry.ResourceAttributeSet(context_id, resource_id, key, val));
			return true;
		} else {
			return false;
		}
	}
	
	public Night.Interface.Resource.Resource get_resource(){
		return resource_writer.get_resource();
	}
}
