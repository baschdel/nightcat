public class Night.Backend.Request.Helper.LoggedStreamWriter : Night.Interface.ResourceStream.Writer, Object {

	public Night.Interface.ResourceStream.Writer resource_writer { get; protected set; }
	public string resource_id { get; protected set; }
	public string context_id { get; protected set; }
	
	public signal void log_event(Night.Interface.Request.Log.Entry entry);
	
	public LoggedStreamWriter(Night.Interface.ResourceStream.Writer resource_writer, string context_id, string resource_id){
		this.resource_writer = resource_writer;
		this.resource_id = resource_id;
		this.context_id = context_id;
		this.resource_writer.awaiting_data.connect(this.on_awaiting_data);
	}

	~LoggedStreamWriter(){
		this.resource_writer.awaiting_data.disconnect(this.on_awaiting_data);
		if (!is_closed()) {
			this.close();
		}
	}
	
	private void on_awaiting_data(){
		this.awaiting_data();
	}

	public bool append(uint8[] data, Cancellable? cancellable = null){
		return resource_writer.append(data, cancellable);
	}
	
	public bool can_write(){
		return resource_writer.can_write();
	}
	
	public bool is_closed(){
		return resource_writer.is_closed();
	}
	
	public bool close(){
		return resource_writer.close();
	}
	
	public bool set_attribute(string key, string val){
		if (resource_writer.set_attribute(key, val)) {
			log_event(new Night.Backend.Request.Log.Entry.ResourceAttributeSet(context_id, resource_id, key, val));
			return true;
		} else {
			return false;
		}
	}
	
}
