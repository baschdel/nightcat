public class Night.Backend.Request.ContextInterface : Night.Interface.Request.ContextInterface, Object {
	
	private Night.Backend.Request.Request _request;
	private Night.Interface.Request.RequestSession session;
	private string? parent_id = null;
	private Night.Interface.Request.ResourceAllocator? resource_allocator = null;
	private string? uri = null;
	private Night.Interface.Request.Action? action = null;
	private Night.Interface.ResourceStream.Receiver? resource = null;
	private string? address = null;
	private Night.Interface.Request.ConnectorCallback? connector_callback = null;
	private string? scheme = null;
	private string? authority = null; //or host, if a host resolver
	private string? port = null;
	
	private string uuid = GLib.Uuid.string_random();
	private string name = "";
	private Night.Interface.Request.ContextRole? role = null;
	private bool _is_open = false;
	private bool was_open = false;
	private Night.Interface.Request.ContextConfiguration? configuration = null;
	
	public ContextInterface.connector(Night.Backend.Request.Request _request, Night.Interface.Request.RequestSession session, string parent_id, string address, owned Night.Interface.Request.ConnectorCallback connector_callback){
		this._request = _request;
		this.session = session;
		this.parent_id = parent_id;
		this.address = address;
		this.connector_callback = (owned) connector_callback;
	}
	
	public ContextInterface.host_resolver(Night.Backend.Request.Request _request, Night.Interface.Request.RequestSession session, string parent_id, string scheme, string hostname, string port){
		this._request = _request;
		this.session = session;
		this.parent_id = parent_id;
		this.scheme = scheme;
		this.authority = hostname;
		this.port = port;
	}
	
	public ContextInterface.authority_resolver(Night.Backend.Request.Request _request, Night.Interface.Request.RequestSession session, string parent_id, string scheme, string authority){
		this._request = _request;
		this.session = session;
		this.parent_id = parent_id;
		this.scheme = scheme;
		this.authority = authority;
	}
	
	public ContextInterface.urn_resolver(Night.Backend.Request.Request _request, Night.Interface.Request.RequestSession session, string parent_id, string urn){
		this._request = _request;
		this.session = session;
		this.parent_id = parent_id;
		this.uri = urn;
	}
	
	public ContextInterface.actor_or_transmission(Night.Backend.Request.Request _request, Night.Interface.Request.RequestSession session, string parent_id, string uri, Night.Interface.Request.Action action, Night.Interface.ResourceStream.Receiver? resource, Night.Interface.Request.ResourceAllocator? resource_allocator = null, string? address = null){
		this._request = _request;
		this.session = session;
		this.parent_id = parent_id;
		this.uri = uri;
		this.action = action;
		this.resource = resource;
		this.resource_allocator = resource_allocator;
		this.address = address;
	}
	
	public ContextInterface.root_actor(Night.Backend.Request.Request _request, Night.Interface.Request.RequestSession session, Night.Interface.Request.ResourceAllocator? resource_allocator = null){
		this._request = _request;
		this.session = session;
		this.uri = _request.get_uri();
		this.action = _request.get_action();
		this.resource = _request.get_attached_resource();
		this.resource_allocator = resource_allocator;
	}
	
	private void on_worker_return(Night.Interface.Request.ContextInterface context, string? returnval){
		if (context.is_open()) {
			if (returnval == null) {
				context.close_because_of_success();
			} else {
				context.close_because_of_error(returnval);
			}
		}
	}
	
	public void call_worker_in_this(Night.Interface.Request.WorkerFunction worker, bool parallel = false){
		if (parallel) {
			new Thread<int>(@"worker: $name $action",() => {
				on_worker_return(this, worker(this));
				return 0;
			});
		} else {
			on_worker_return(this, worker(this));
		}
	}
	 //////////////////////////////
	// Resource Output Callbacks
	
	public void mark_resource_as_success(string resource_id){
		if(!_is_open){ return; }
		this._request.mark_resource_as_success(this.uuid, resource_id);
	}
	
	public void mark_resource_as_errored(string resource_id, string most_significant_error){
		if(!_is_open){ return; }
		this._request.mark_resource_as_errored(this.uuid, resource_id, most_significant_error);
	}
	
	  ////////////////////////////////////////////////
	 //  Night.Interface.Request.ContextInterface  //
	////////////////////////////////////////////////

	//introduce yourself
	
	public bool initalize_interface(string name, Night.Interface.Request.ContextRole role){
		if (this.was_open) { return false; }
		this.name = name;
		this.role = role;
		switch(role.get_context_type()) {
			case Night.Interface.Request.ContextType.TRANSMISSION:
				if (this.uri == null){ return false; }
				if (this.action == null){ return false; }
				if (this.address == null) { return false; }
				this.configuration = session.get_transmission_configuration(this.parent_id, this._request, this.name, this.role, this.uri, this.address, this.action, this.resource);
				break;
			case Night.Interface.Request.ContextType.ACTOR:
				if (this.uri == null){ return false; }
				if (this.action == null){ return false; }
				this.configuration = session.get_actor_configuration(this.parent_id, this._request, this.name, this.role, this.uri, this.action, this.resource);
				break;
			case Night.Interface.Request.ContextType.RESOLVER:
				switch(role) {
					case Night.Interface.Request.ContextRole.HOST_RESOLVER:
						if (this.scheme == null){ return false; }
						if (this.authority == null){ return false; }
						if (this.port == null){ return false; }
						this.configuration = session.get_host_resolver_configuration(this.parent_id, this._request, this.name, this.role, this.scheme, this.authority, this.port);
						break;
					case Night.Interface.Request.ContextRole.AUTHORITY_RESOLVER:
						if (this.scheme == null){ return false; }
						if (this.authority == null){ return false; }
						if (this.port != null){ return false; } //make sure this wasn't supposed to be a host resolver
						this.configuration = session.get_authority_resolver_configuration(this.parent_id, this._request, this.name, this.role, this.scheme, this.authority);
						break;
					case Night.Interface.Request.ContextRole.URN_RESOLVER:
						if (this.uri == null){ return false; }
						this.configuration = session.get_urn_resolver_configuration(this.parent_id, this._request, this.name, this.role, this.uri);
						break;
					default:
						return false;
				}
				break;
			case Night.Interface.Request.ContextType.CONNECTOR:
				if (this.address == null){ return false; }
				this.configuration = session.get_connection_configuration(this.parent_id, this._request, this.name, this.role, this.address);
				break;
			default:
				return false;
		}
		this._request.open_context(this.uuid, name, this.parent_id, role, this.configuration);
		this._is_open = true;
		this.was_open = true;
		string? configuration_error = this.configuration.get_value("error");
		if (configuration_error != null){
			close_because_of_error("error."+configuration_error);
			return false;
		}
		return true;
	}
	
	//get information about the interface
	
	public bool is_open(){
		return _is_open;
	}
	
	public bool close_requested(){
		if (this.parent_id == null) { return _request.get_cancellable_object().is_cancelled(); }
		return (!_request.is_context_open(this.parent_id)) || _request.get_cancellable_object().is_cancelled();
	}
	
	//get information about yourself
	
	public string get_my_name(){
		return this.name;
	}
	
	public Night.Interface.Request.ContextRole get_my_role(){
		if (this.role == null) {
			return Night.Interface.Request.ContextRole.REQUEST_ROUTER;
		}	else {
			return this.role;
		}
	}
	
	public string get_my_id(){
		return this.uuid;
	}
	
	public string? get_my_parents_id(){
		return this.parent_id;
	}
	
	//get information about your job
	public Night.Interface.Request.ContextConfiguration get_my_configuration(){ //also contais uri, action etc.
		if (this.configuration == null){
			print("returning empty configuration\n");
			return new Night.Backend.Request.ContextConfiguration().empty_seal();
		} else {
			return this.configuration;
		}
	}
	
	public Night.Interface.ResourceStream.Receiver? get_argument_resource(){
		return this.resource;
	}
	
	//get information about others
	
	public Night.Interface.Request.Request get_request(){
		return this._request;
	}
	
	//submit information 
	
	public void submit_feedback(string key, string val){
		if(!_is_open){ return; }
		this._request.submit_feedback(this.uuid, key, val);
	}
	
	public bool submit_connection(GLib.IOStream connection, bool always_close) throws Error{ //returns true when the connection could be reused
		if(!_is_open){ return false; }
		if (connector_callback != null){
			submit_feedback("connection_state","open");
			bool reuseable = connector_callback(connection);
			if (connection.is_closed()){
				submit_feedback("connection_state","closed");
				return false;
			}
			if (!reuseable || always_close){
				submit_feedback("connection_state","closing");
				try {
					connection.close();
				} catch (Error e) {
					submit_feedback("error.exception", e.message);
				}
				submit_feedback("connection_state","closed");
			} else {
				submit_feedback("connection_state","reuseable");
			}
			return reuseable;
		} else {
			return false;
		}
	}
	
	/*
	public Night.Interface.Resource.Writer? submit_logged_resource(Night.Interface.Resource.Writer writer, string uri){
		if(!_is_open){ return null; }
		return _request.submit_logged_resource(this.uuid, writer, uri);
	}
	*/
	
	public void set_most_significant_child(string? context_uuid){
		if(!_is_open){ return; }
		this._request.set_most_significant_child(this.uuid, context_uuid);
	}
	
	//close the context
	
	public void close_because_of_success(){
		if(!_is_open){ return; }
		this._request.close_context_because_of_success(this.uuid);
		this._is_open = false;
	}
	
	public void close_because_of_error(string most_significant_error){
		if(!_is_open){ return; }
		this._request.close_conetxt_because_of_error(this.uuid, most_significant_error);
		_is_open = false;
	}
	//will set the <most_significant_error> feedback value to "true" if it isn't already set
	
	//call for help
	
	/*
	public Night.Interface.Request.Request make_subrequest(string uri, Night.Interface.Request.Action action, Night.Interface.ResourceStream.Receiver? resource, bool reload = false){
		var request = this.session.make_subrequest(this.uuid, this._request, uri, action, resource, reload);
		this._request.log_subrequest(this.uuid, request);
		return request;
	}
	*/
	
	public bool open_connection(string address, Night.Interface.Request.ConnectionCallback callback, out string context_uuid = null, Night.Interface.Request.Connector? connector = null){
		var _connector = connector;
		if (_connector == null){
			_connector = session.get_connector(this.uuid, this._request, address);
		}
		bool connection_established = false;
		var context = new ContextInterface.connector(this._request, this.session, this.uuid, address, (connection) => {
			connection_established = true;
			return callback(this, connection);
		});
		context_uuid = context.get_my_id();
		on_worker_return(context, _connector.request_connection(context));
		return connection_established;
	}
	
	public string call_worker(Night.Interface.Request.WorkerFunction worker, string uri, Night.Interface.Request.Action action, Night.Interface.ResourceStream.Receiver? resource = null, bool parallel = false, Night.Interface.Request.ResourceAllocator? resource_allocator = null, string? address = null){ //if the parallel flag is set the worker will be run in a new threat
		var context = this.create_context_interface(uri, action, resource, resource_allocator, address);
		if (parallel) {
			new Thread<int>(@"worker: $name $action",() => {
				on_worker_return(context, worker(context));
				return 0;
			});
		} else {
			on_worker_return(context, worker(context));
		}
		return context.get_my_id();
	}
	
	public Night.Interface.Request.ContextInterface create_context_interface(string uri, Night.Interface.Request.Action action, Night.Interface.ResourceStream.Receiver? resource = null, Night.Interface.Request.ResourceAllocator? resource_allocator = null, string? address = null){
		var _resource_allocator = resource_allocator;
		if (_resource_allocator == null){ resource_allocator = this.resource_allocator; }
		return new ContextInterface.actor_or_transmission(this._request, this.session, this.uuid, uri, action, resource, resource_allocator, address);
	}
	
	public Night.Interface.Request.NameResolverResult resolve_host(string scheme, string hostname, string port, Night.Interface.Request.NameResolver? resolver = null){
		var _resolver = resolver;
		if (_resolver == null){
			_resolver = session.get_host_resolver(scheme);
		}
		var context = new ContextInterface.host_resolver(this._request, this.session, this.uuid, scheme, hostname, port);
		return _resolver.lookup(context);
	}
	
	public Night.Interface.Request.NameResolverResult resolve_authority(string scheme, string authority, Night.Interface.Request.NameResolver? resolver = null){
		var _resolver = resolver;
		if (_resolver == null){
			_resolver = session.get_authority_resolver(scheme);
		}
		var context = new ContextInterface.authority_resolver(this._request, this.session, this.uuid, scheme, authority);
		return _resolver.lookup(context);
	}
	
	public Night.Interface.Request.NameResolverResult resolve_urn(string urn, Night.Interface.Request.NameResolver? resolver = null){
		var _resolver = resolver;
		if (_resolver == null){
			_resolver = session.get_urn_resolver(urn);
		}
		var context = new ContextInterface.urn_resolver(this._request, this.session, this.uuid, urn);
		return _resolver.lookup(context);
	}
	
	public Night.Interface.Request.ContextInterface? create_resolver_subinterface(){
		switch(this.get_my_role()){
			case HOST_RESOLVER:
				return  new ContextInterface.host_resolver(this._request, this.session, this.uuid, this.scheme, this.authority, this.port);
			case AUTHORITY_RESOLVER:
				return new ContextInterface.authority_resolver(this._request, this.session, this.uuid, this.scheme, this.authority);
			case URN_RESOLVER:
				return new ContextInterface.urn_resolver(this._request, this.session, this.uuid, this.uri);
			default:
				return null;
		}
	}
	
	public Night.Interface.Request.ResourceStreamOutput? allocate_logged_resource(string uri,  Night.Interface.Request.ResourceAllocator? resource_allocator = null){
		var _resource_allocator = resource_allocator;
		if (_resource_allocator == null) { _resource_allocator = this.resource_allocator; }
		if (_resource_allocator == null) { return null; }
		var resource_writer = this.resource_allocator.allocate_resource_for_request(this._request, this.uuid, uri);
		string output_id;
		var logged_writer = _request.submit_logged_resource_stream(this.uuid, resource_writer, uri, out output_id);
		if (logged_writer == null) { return null; }
		return new Might.Backend.Request.ResourceStreamOutput(logged_writer, this, output_id);
	}
	
}
