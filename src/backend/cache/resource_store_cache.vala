public class Night.Backend.Cache.ResourceStoreCache : Night.Interface.Cache.Cache, Object {
	
	protected Night.Interface.Resource.Store resource_store;
	protected string prefix;
	
	protected HashTable<string,Night.Interface.Cache.Info> resource_information = new  HashTable<string,Night.Interface.Cache.Info>(str_hash, str_equal);
	
	public ResourceStoreCache(Night.Interface.Resource.Store resource_store, string prefix){
		this.resource_store = resource_store;
		this.prefix = prefix;
	}
	
	~ResourceStoreCache(){
		foreach_item((cache_id) => {
			remove(cache_id);
		});
	}
	
	protected string get_resource_name(string cache_id){
		return prefix+".item."+cache_id;
	}
	
	  ///////////////////////////////////
	 //  Night.Interface.Cache.Cache  //
	///////////////////////////////////
	
	public void foreach_item(Func<string> cb){
		resource_information.foreach((k,_) => {
			cb(k);
		});
	}
	
	public void foreach_uri(string uri, bool include_expired, Func<string> cb){
		resource_information.foreach((k,info) => {
			if (info.uri == uri) {
				if (include_expired || !info.is_expired() || info.pinned) {
					cb(k);
				}
			}
		});
	}
	
	public void foreach_expired(bool include_pinned, Func<string> cb){
		resource_information.foreach((k,info) => {
			if (info.is_expired()) {
				if (include_pinned || !info.pinned) {
					cb(k);
				}
			}
		});
	}
	
	public Night.Interface.Cache.StoreReturn? store(Night.Interface.Cache.Info info){
		string uuid = GLib.Uuid.string_random();
		string name = get_resource_name(uuid);
		if (!resource_store.create_resource(name)) { return null; }
		var writer = resource_store.retrieve_resource_writer(name);
		if (writer == null) {
			resource_store.delete_resource(name);
			return null;
		}
		return new Night.Interface.Cache.StoreReturn(uuid, new Night.Interface.ResourceStream.ResourceWriter(writer), this.read(uuid));
	}
	
	public Night.Interface.ResourceStream.Receiver? read(string cache_id){
		if (get_info(cache_id) == null) { return null; }
		var resource = resource_store.retrieve_resource(get_resource_name(cache_id));
		if (resource == null) { return null; }
		return new Night.Interface.ResourceStream.ResourceReceiver(resource);
	}
	
	public Night.Interface.Cache.Info? get_info(string cache_id){
		return resource_information.get(cache_id);
	}
	
	public bool pin(string cache_id){
		var info = resource_information.get(cache_id);
		if (info != null) {
			if (!info.pinned) {
				resource_information.set(cache_id, new Night.Interface.Cache.Info(info.uri, info.time_downloaded_utc, info.expiry_date_utc, true));
				return true;
			}
		}
		return false;
	}
	
	public bool remove(string cache_id){
		bool success = resource_store.delete_resource(get_resource_name(cache_id));
		if (success) {
			resource_information.remove(cache_id);
		}
		return success;
	}
	
}
