public class Night.Backend.Protocol.Finger.Fetcher : Night.Interface.Request.Fetcher, Object {
	
	public bool can_handle_uri(string uri, Night.Interface.Request.Action action){
		// do not test for finger:// to allow for things like finger+whatever
		return uri.has_prefix("finger") && action == Night.Interface.Request.Action.DOWNLOAD;
	}
	
	public string? request(Night.Interface.Request.ContextInterface context){
		context.initalize_interface("night.fetcher.finger", Night.Interface.Request.ContextRole.FETCHER);
		var config = context.get_my_configuration();
		string? uri = config.get_value("uri");
		string? action = config.get_value("action");
		if (uri == null) { return "error.no_uri"; }
		if (!uri.has_prefix("finger")) { return "error.invalid_uri"; }
		var request = context.get_request();
		if (action == "download"){
			var parsed_uri = new Night.Util.ParsedUri(uri,false);
			var resolver = context.resolve_authority(parsed_uri.scheme, parsed_uri.authority);
			string? address;
			bool success = false;
			while ((address = resolver.next()) != null) {
				if (context.close_requested()) {
					context.close_because_of_error("error.cancelled");
				}
				string context_uuid = context.call_worker(Fetcher.download_transmission_worker, uri, Night.Interface.Request.Action.DOWNLOAD, null, false, null, address);
				if (request.get_context_successful(context_uuid)){
					context.set_most_significant_child(context_uuid);
					success = true;
					break;
				}
			}
			resolver.close();
			if (success) {
				return null;
			} else {
				return "error.all_transmissions_failed";
			}
		} else {
			return "error.invalid_action";
		}
	}
	
	public static string? download_transmission_worker(Night.Interface.Request.ContextInterface context){
		context.initalize_interface("night.transmission.finger", Night.Interface.Request.ContextRole.DOWNLOAD);
		var config = context.get_my_configuration();
		string? uri = config.get_value("uri");
		string? address = config.get_value("address");
		string? action = config.get_value("action");
		if (uri == null) { return "error.no_uri"; }
		if (address == null) { return "error.no_address"; }
		if (action == "download"){
			string context_uuid;
			bool connection_established = context.open_connection(address, download_connection_established_callback, out context_uuid);
			context.set_most_significant_child(context_uuid);
			if (connection_established) {
				return null;
			} else {
				return "error.connection_failed";
			}
		} else {
			return "error.invalid_action";
		}
	}
	
	public static bool download_connection_established_callback(Night.Interface.Request.ContextInterface context, GLib.IOStream connection) throws Error {
		var configuration = context.get_my_configuration();
		string? uri = configuration.get_value("uri");
		string? address = configuration.get_value("address");
		if (uri == null) {
			context.close_because_of_error("error.no_uri");
			return true;
		}
		if (address == null) {
			context.close_because_of_error("error.no_address");
			return true;
		}
		uint64? block_size = configuration.get_number("block_size");
		uint64? size_limit = configuration.get_number("size_limit");
		if (block_size == null) { block_size = 1024*64; }
		if (size_limit == null) { size_limit = 1024*1024*1024*3; }
		
		var parsed_uri = new Night.Util.ParsedUri(uri,false);
		
		string query = "";
		if (parsed_uri.username != null) {
			query = parsed_uri.username;
		} else if (parsed_uri.path != null && parsed_uri.path != "/" && parsed_uri.path != "") {
			query = Uri.unescape_string(parsed_uri.path.substring(1),"\n\r\0");
		}
		context.submit_feedback("finger.query", query);
		
		var resource_output = context.allocate_logged_resource(uri);
		if (resource_output == null) {
			context.close_because_of_error("error.resource_allocation_failed");
			return true;
		}
		var resource_writer = resource_output.get_writer();
		resource_writer.set_attribute("uri",uri);
		resource_writer.set_attribute("mimetype","text/plain");
		resource_writer.set_attribute("host_address", address);
		resource_writer.set_attribute("known_incomplete","true");
		
		try {
			connection.output_stream.write(@"$query\r\n".data);
			var input_stream = new DataInputStream (connection.input_stream);
			
			uint64 counter = 0;
			while (true){
				if (context.close_requested()) {
					resource_writer.close();
					context.close_because_of_error("error.cancelled");
					return false;
				}
				var bytes = input_stream.read_bytes((size_t) block_size);
				counter += bytes.length;
				if (bytes.length == 0) {
					break;
				} else {
					if (!resource_writer.append(Bytes.unref_to_data(bytes))) {
						resource_writer.close();
						resource_output.mark_as_errored("error.resource_write_failed");
						context.close_because_of_error("error.resource_write_failed");
						return false;
					}
				}
				//terminate early if file gets too big
				if (counter > size_limit) {
					resource_writer.close();
					resource_output.mark_as_errored("error.size_limit_reached");
					context.close_because_of_error("error.size_limit_reached");
					return false;
				}
			}
			resource_writer.set_attribute("known_incomplete","false");
			resource_writer.close();
			resource_output.mark_as_success();
			return false;
			
		} catch (Error e) {
			if (resource_writer != null) {
				resource_writer.close();
				resource_output.mark_as_errored("error.connection_failed");
			}
			context.submit_feedback("error.exception", e.message);
			context.close_because_of_error("error.exception");
			throw e;
		}
	}
	
}
