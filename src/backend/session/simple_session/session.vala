public class Night.Backend.Session.SimpleSession.Session :  Night.Interface.Session.Session, Night.Interface.Request.RequestSession, Object {
	
	private string uuid = GLib.Uuid.string_random();
	private string _name = "Simple Session";
	private Night.Interface.Request.Connector _connector = new Night.Backend.NopWorker.Connector();
	private Night.Interface.Request.NameResolver _urn_resolver = Night.Backend.NopWorker.Resolver.instance;
	private Night.Interface.Request.NameResolver _authority_resolver;
	private Night.Interface.Request.NameResolver _host_resolver;
	private Night.Backend.Session.SimpleSession.FetcherManager fetcher_manager = new Night.Backend.Session.SimpleSession.FetcherManager(new Night.Backend.NopWorker.Fetcher("error.unknown_uri_scheme"));
	
	public Night.Interface.Request.Connector tls_connector = new Night.Backend.Connector.Tls();
	
	public Night.Backend.AuthorityResolver.Configuration authority_resolver_configuration = new Night.Backend.AuthorityResolver.Configuration();
	public Night.Backend.HostResolver.Configuration host_resolver_configuration = new Night.Backend.HostResolver.Configuration();
	
	public bool multithreaded = false;
	
	public Session(){
		_authority_resolver = new Night.Backend.AuthorityResolver.Multiplexer(authority_resolver_configuration);
		_host_resolver = new Night.Backend.HostResolver.Multiplexer(host_resolver_configuration);
	}
	
	private void fetch(Night.Backend.Request.Request request){
		var parsed_uri = new Night.Util.ParsedUri(request.get_uri());
		string? scheme = parsed_uri.scheme;
		if (scheme == null){ scheme = ""; }
		var context = new Night.Backend.Request.ContextInterface.root_actor(request, this, request);
		context.call_worker_in_this(fetcher_manager.get_fetcher_for(scheme).request, this.multithreaded);
	}
	
	public void set_urn_resolver(Night.Interface.Request.NameResolver urn_resolver){
		this._urn_resolver = urn_resolver;
	}
	
	public void set_authority_resolver(Night.Interface.Request.NameResolver authority_resolver){
		this._authority_resolver = authority_resolver;
	}
	
	public void set_host_resolver(Night.Interface.Request.NameResolver host_resolver){
		this._host_resolver = host_resolver;
	}
	
	  //////////////////////////////////////////////
	 //  Night.Interface.Request.RequestSession  //
	//////////////////////////////////////////////
	
	/*
	public Night.Interface.Request.Request make_subrequest(string context_uuid, Night.Interface.Request.Request request, string uri, Night.Interface.Request.Action action, Night.Interface.Resource.Resource? resource, bool reload = false){
		switch(action){
			case UPLOAD:
				return this.request_upload(uri, resource);
			case DELETE:
				return this.request_deletion(uri);
			case EVENTCONNECT:
				return this.request_event_channel(uri);
			case DOWNLOAD:
			default:
				return this.request_download(uri, reload);
		}
	}
	*/
	
	public Night.Interface.Request.Connector get_connector(string parent_context_uuid, Night.Interface.Request.Request request, string address){
		if (address.has_prefix("!tls,")) { return tls_connector; }
		return this._connector;
	}
	
	public Night.Interface.Request.NameResolver get_urn_resolver(string urn){
		return this._urn_resolver;
	}
	
	public Night.Interface.Request.NameResolver get_authority_resolver(string scheme){
		return this._authority_resolver;
	}
	
	public Night.Interface.Request.NameResolver get_host_resolver(string scheme){
		return this._host_resolver;
	}
	
	//if the types do not match the session may return a configuration with an error = type_mismatch key-value pair
	//the session also has to set the uri, address and action keys and must seal the cofiguration.
	public Night.Interface.Request.ContextConfiguration get_actor_configuration(string? parent_context_uuid, Night.Interface.Request.Request request, string name, Night.Interface.Request.ContextRole role, string uri, Night.Interface.Request.Action action, Night.Interface.ResourceStream.Receiver? resource = null){
		var config = new Night.Backend.Request.ContextConfiguration();
		config.set_value("uri",uri);
		config.set_value("action",action.to_string());
		config.seal();
		return config;
	}
	
	public Night.Interface.Request.ContextConfiguration get_transmission_configuration(string? parent_context_uuid, Night.Interface.Request.Request request, string name, Night.Interface.Request.ContextRole role, string uri, string address, Night.Interface.Request.Action action, Night.Interface.ResourceStream.Receiver? resource = null){
		var config = new Night.Backend.Request.ContextConfiguration();
		config.set_value("uri",uri);
		config.set_value("address",address);
		config.set_value("action",action.to_string());
		config.seal();
		return config;
	}
	
	public Night.Interface.Request.ContextConfiguration get_connection_configuration(string parent_context_uuid, Night.Interface.Request.Request request, string name, Night.Interface.Request.ContextRole role, string address){
		var config = new Night.Backend.Request.ContextConfiguration();
		config.set_value("address",address);
		config.seal();
		return config;
	}
	
	public Night.Interface.Request.ContextConfiguration get_host_resolver_configuration(string parent_context_uuid, Night.Interface.Request.Request request, string name, Night.Interface.Request.ContextRole role, string scheme, string hostname, string port){
		var config = new Night.Backend.Request.ContextConfiguration();
		config.set_value("scheme",scheme);
		config.set_value("hostname",hostname);
		config.set_value("port",port);
		config.seal();
		return config;
	}
	
	public Night.Interface.Request.ContextConfiguration get_authority_resolver_configuration(string parent_context_uuid, Night.Interface.Request.Request request, string name, Night.Interface.Request.ContextRole role, string scheme, string authority){
		var config = new Night.Backend.Request.ContextConfiguration();
		config.set_value("scheme",scheme);
		config.set_value("authority",authority);
		if (scheme == "gopher") {
			config.set_value("default_port","70");
		}
		config.seal();
		return config;
	}
	
	public Night.Interface.Request.ContextConfiguration get_urn_resolver_configuration(string parent_context_uuid, Night.Interface.Request.Request request, string name, Night.Interface.Request.ContextRole role, string urn){
		var config = new Night.Backend.Request.ContextConfiguration();
		config.set_value("urn",urn);
		config.seal();
		return config;
	}
	
	  ///////////////////////////////////////
	 //  Night.Interface.Session.Session  //
	///////////////////////////////////////
	
	public string get_session_uuid(){
		return this.uuid;
	}
	
	public bool set_fetcher(Night.Interface.Request.Fetcher? fetcher, string? scheme = null){
		this.fetcher_manager.set_fetcher(fetcher,scheme);
		return true;
	}
	
	public bool set_connector(Night.Interface.Request.Connector connector){
		this._connector = connector;
		return true;
	}
	
	public Night.Interface.Request.Request request_download(Night.Interface.Request.RequestContext context, string uri, bool reload=false){
		var request = new Night.Backend.Request.Request(uri, Night.Interface.Request.Action.DOWNLOAD, context);
		this.fetch(request);
		return request;
	}
	
	public Night.Interface.Request.Request request_upload(Night.Interface.Request.RequestContext context, string uri, Night.Interface.ResourceStream.Receiver resource){
		var request = new Night.Backend.Request.Request(uri, Night.Interface.Request.Action.UPLOAD, context, resource);
		this.fetch(request);
		return request;
	}
	
	public Night.Interface.Request.Request request_deletion(Night.Interface.Request.RequestContext context, string uri){
		var request = new Night.Backend.Request.Request(uri, Night.Interface.Request.Action.DELETE, context);
		this.fetch(request);
		return request;
	}
	
	public Night.Interface.Request.Request request_event_channel(Night.Interface.Request.RequestContext context, string uri){
		var request = new Night.Backend.Request.Request(uri, Night.Interface.Request.Action.EVENTCONNECT, context);
		this.fetch(request);
		return request;
	}
	
	public Night.Interface.Request.NameResolver resolve_urn(string urn){
		return Night.Backend.NopWorker.Resolver.instance;
	}
	
	public Night.Interface.Request.NameResolver resolve_authority(string scheme, string authority){
		return Night.Backend.NopWorker.Resolver.instance;
	}
	
	public void set_name(string name){
		this._name = name;
	}
	
	public string get_name(){
		return _name;
	}
	
}
