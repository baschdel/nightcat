public class Night.Backend.Resolver.GLibDnsResolver : Night.Interface.Request.NameResolver, Object {
	
	public Night.Interface.Request.NameResolverResult lookup(Night.Interface.Request.ContextInterface context){
		return new Nigh.TestSession.GLibDnsResolverResult(context);
	}	
	
}

public class Nigh.TestSession.GLibDnsResolverResult : Night.Interface.Request.NameResolverResult, Object {
	
	private Night.Util.Stack<string> addresses = new Night.Util.Stack<string>();
	private bool closed = false;
	private Night.Interface.Request.ContextInterface context;
	
	public GLibDnsResolverResult(Night.Interface.Request.ContextInterface context){
		this.context = context;
		context.initalize_interface("night.resolver.glib_dns_resolver", Night.Interface.Request.ContextRole.HOST_RESOLVER);
		var configuration = context.get_my_configuration();
		string? host = configuration.get_value("hostname");
		string? port = configuration.get_value("port");
		if (host == null) {
			context.close_because_of_error("error.no_host_specified");
			return;
		}
		if (port == null) {
			context.close_because_of_error("error.no_port_specified");
			return;
		}
		try {
			var resolver = Resolver.get_default ();
			var inet_adresses = resolver.lookup_by_name (host, null);
			string str_address;
			foreach (InetAddress address in inet_adresses){
				str_address = Uri.escape_string(address.to_string(),":");
				if (str_address.index_of_char(':') < 0){ //prefer IPv4 connections
					addresses.push(@"!tcp+ipv4,$port,$str_address");
				} else {
					addresses.fifo_push(@"!tcp+ipv6,$port,$str_address");
				}
			}
		} catch (Error e) {
			closed = true;
			context.submit_feedback("error.exception",e.message);
			context.close_because_of_error("error.cant_resolve");
		}
	}
	
	//if the next function returns null this means there are no entrys left and the resolver will close automatically, the next function will block for the time the lookup takes
	public string? next(out Night.Interface.Request.NameCategory category = null, out int rating = null){
		category = Night.Interface.Request.NameCategory.NEUTRAL;
		rating = 0;
		if (!context.is_open()) { return null; }
		if (context.close_requested()) {
			context.close_because_of_error("cancelled");
			return null;
		}
		if (closed) { return null; }
		string? address = addresses.pop();
		if (address == null) {
			close();
			return null;
		}
		if (address.index_of_char(':') < 0){
			rating = 10;
		}
		context.submit_feedback("resolver.current_address",address);
		return address;
	}
	
	public void close(){
		closed = true;
		context.close_because_of_success();
	}
	
	public bool is_closed(){
		return closed;
	}
	
}
