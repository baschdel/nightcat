public class Night.Backend.Library.Action : Night.Interface.Library.Action, Object {
	
	private string action_name;
	private Night.Interface.Library.ActionCallback? callback;
	private Night.Backend.Library.Context context;
	
	public Action(string action_name, owned Night.Interface.Library.ActionCallback callback, Night.Backend.Library.Context context){
		this.action_name = action_name;
		this.callback = (owned) callback;
		this.context = context;
		context.action_disabled.connect(on_action_disabled);
		context.all_actions_disabled.connect(do_disconnect);
	}
	
	private void on_action_disabled(string action_name){
		if (action_name == this.action_name) {
			do_disconnect();
		}
	}
	
	private void do_disconnect(){
		lock(callback) {
			callback = null;
			context = null;
			context.action_disabled.disconnect(on_action_disabled);
			context.all_actions_disabled.disconnect(do_disconnect);
		}
	}
	
	  ////////////////////////////////////
	 // Night.Interface.Library.Action //
	////////////////////////////////////
	
	//WARNING: Don't remove the callback while it's being called, deadlock!
	public bool trigger_action(string? argument){ //returns true on success
		lock(callback) {
			if (callback != null) {
				if (callback(argument)) {
					context.on_action_triggered(action_name, argument);
					return true;
				}
			}
		}
		return false;
	}
	
	public bool action_avaiable(){
		return callback != null;
	}
	
}
