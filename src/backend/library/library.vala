public class Night.Backend.Library.Library : Night.Interface.Library.Library, Object {
	
	protected Night.Interface.Library.Librarian librarian;
	protected Night.Interface.Library.Library outgoing_library;
	protected string context_name;
	
	public Library(Night.Interface.Library.Librarian librarian, Night.Interface.Library.Library outgoing_library, string context_name){
		this.librarian = librarian;
		this.outgoing_library = outgoing_library;
		this.context_name = context_name;
	}
	
	  /////////////////////////////////////
	 // Night.Interface.Library.Library //
	/////////////////////////////////////
	
	public virtual void request(Night.Interface.Library.CallbackHandler callback_handler, Night.Interface.Library.Verb verb, string uri, string reason){
		librarian.request(new Night.Backend.Library.Context(verb, uri, reason, callback_handler, outgoing_library, context_name));
	}
	
}
