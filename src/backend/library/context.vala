public class Night.Backend.Library.Context : Night.Interface.Library.Context, Night.Interface.Library.LogListener, Night.Backend.Library.WriterLogger, Object {
	
	private string _uri;
	private Night.Interface.Library.Verb _verb;
	private string _reason;
	private bool _is_open = true;
	private Night.Interface.Library.CallbackHandler callback_handler;
	private Night.Interface.Library.Library library;
	private string context_id = GLib.Uuid.string_random();
	private Night.Interface.Library.ContextType context_type = Night.Interface.Library.ContextType.LIBRARY;
	
	public signal void action_disabled(string action_name);
	public signal void all_actions_disabled();
	
	public Context(Night.Interface.Library.Verb verb, string uri, string reason, Night.Interface.Library.CallbackHandler callback_handler, Night.Interface.Library.Library library, string context_name){
		_verb = verb;
		_uri = uri;
		_reason = reason;
		this.callback_handler = callback_handler;
		this.library = library;
		append_log_entry(new Night.Interface.Library.LogEntry.context_opened(context_id, context_type, context_name, uri).because(reason));
	}
	
	public void on_action_triggered(string action_name, string argument){
		append_log_entry(new Night.Interface.Library.LogEntry.action_triggered(context_id, context_type, action_name, argument));
	}
	
	  /////////////////////////////////////////
	 // Night.Interface.Library.LogListener //
	/////////////////////////////////////////
	
	public void append_log_entry(Night.Interface.Library.LogEntry entry){
		callback_handler.append_log_entry(entry);
	}
	
	  ////////////////////////////////////////
	 // Night.Backend.Library.WriterLogger //
	////////////////////////////////////////
	
	public void log_writer_feedback(string resource_id, string key, string val){
		append_log_entry(new Night.Interface.Library.LogEntry.resource_feedback(context_id, context_type, resource_id, key, val));
	}
	
	public void log_writer_closed(string resource_id){
		append_log_entry(new Night.Interface.Library.LogEntry.resource_writer_closed(context_id, context_type, resource_id));
	}
	
	  /////////////////////////////////////
	 // Night.Interface.Library.Context //
	/////////////////////////////////////
	
	public string get_uri(){
		return _uri;
	}
	
	public Night.Interface.Library.Verb get_verb(){
		return _verb;
	}
	
	public string get_reason(){
		return _reason;
	}
	
	public bool is_open(){
		return _is_open;
	}
	
	public void submit_feedback(string key, string? val){
		append_log_entry(new Night.Interface.Library.LogEntry.feedback(context_id, context_type, key, val));
	}
	
	public void provide_action(string action_name, owned Night.Interface.Library.ActionCallback callback, string argument_type){
		var action = new Night.Backend.Library.Action(action_name, (owned) callback, this);
		append_log_entry(new Night.Interface.Library.LogEntry.action_provided(context_id, context_type, action, action_name, argument_type));
	}
	
	public void disable_action(string action_name){
		action_disabled(action_name);
		append_log_entry(new Night.Interface.Library.LogEntry.action_unavaiable(context_id, context_type, action_name));
	}
	
	public Night.Interface.ResourceStream.Writer? request_writer_for_downloading(string resource_name){
		string resource_id = GLib.Uuid.string_random();
		append_log_entry(new Night.Interface.Library.LogEntry.resource_writer_requested(context_id, context_type, resource_id, resource_name));
		var writer = callback_handler.request_writer_for_downloading(resource_id, resource_name);
		if (writer == null) {
			append_log_entry(new Night.Interface.Library.LogEntry.resource_writer_denyed(context_id, context_type, resource_id));
		} else {
			writer = new Night.Backend.Library.LoggedWriter(writer, this, resource_id);
			append_log_entry(new Night.Interface.Library.LogEntry.resource_writer_delivered(context_id, context_type, resource_id));
		}
		return writer;
	}
	
	public Night.Interface.ResourceStream.Reader? request_reader_for_uploading(string resource_name){
		string resource_id = GLib.Uuid.string_random();
		append_log_entry(new Night.Interface.Library.LogEntry.resource_reader_requested(context_id, context_type, resource_id, resource_name));
		var reader = callback_handler.request_reader_for_uploading(resource_id, resource_name);
		if (reader == null) {
			append_log_entry(new Night.Interface.Library.LogEntry.resource_reader_denyed(context_id, context_type, resource_id));
		} else {
			append_log_entry(new Night.Interface.Library.LogEntry.resource_reader_delivered(context_id, context_type, resource_id));
		}
		return reader;
	}
	
	public void request_download(Night.Interface.Library.CallbackHandler callback_handler, string uri, string reason, bool reload){
		Night.Interface.Library.Verb verb = Night.Interface.Library.Verb.DOWNLOAD;
		if (reload) { verb = Night.Interface.Library.Verb.RELOAD; }
		var _callback_handler = new Night.Backend.Library.CallbackHandlerSplitter(callback_handler, this);
		library.request(_callback_handler, verb, uri, reason);
	}
	
	public void request_upload(Night.Interface.Library.CallbackHandler callback_handler, string uri, string reason){
		Night.Interface.Library.Verb verb = Night.Interface.Library.Verb.UPLOAD;
		var _callback_handler = new Night.Backend.Library.CallbackHandlerSplitter(callback_handler, this);
		library.request(_callback_handler, verb, uri, reason);
	}
	
	public void request_deletion(Night.Interface.Library.CallbackHandler callback_handler, string uri, string reason){
		Night.Interface.Library.Verb verb = Night.Interface.Library.Verb.DELETE;
		var _callback_handler = new Night.Backend.Library.CallbackHandlerSplitter(callback_handler, this);
		library.request(_callback_handler, verb, uri, reason);
	}
	
	public void close(string? most_significant_error, string? most_significant_error_value){
		_is_open = false;
		all_actions_disabled();
		if (most_significant_error == null) {
			append_log_entry(new Night.Interface.Library.LogEntry.context_closed_success(context_id, context_type));
		} else {
			if (most_significant_error_value != null){
				this.submit_feedback(most_significant_error, most_significant_error_value);
			}
			append_log_entry(new Night.Interface.Library.LogEntry.context_closed_error(context_id, context_type, most_significant_error, most_significant_error_value));
		}
	}
	
}
