public interface Night.Backend.Library.WriterLogger : Object {
	
	public abstract void log_writer_feedback(string resource_id, string key, string val);
	public abstract void log_writer_closed(string resource_id);
	
}
