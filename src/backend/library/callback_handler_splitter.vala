public class Night.Backend.Library.CallbackHandlerSplitter : Night.Interface.Library.CallbackHandler, Night.Interface.Library.LogListener, Object {
	
	private Night.Interface.Library.CallbackHandler callback_handler;
	private Night.Interface.Library.LogListener logger;
	
	public CallbackHandlerSplitter(Night.Interface.Library.CallbackHandler callback_handler, Night.Interface.Library.LogListener logger){
		this.callback_handler = callback_handler;
		this.logger = logger;
	}
	
	  /////////////////////////////////////////
	 // Night.Interface.Library.LogListener //
	/////////////////////////////////////////
	
	public void append_log_entry(Night.Interface.Library.LogEntry entry){
		callback_handler.append_log_entry(entry);
		logger.append_log_entry(entry);
	}
	
	  /////////////////////////////////////////////
	 // Night.Interface.Library.CallbackHandler //
	/////////////////////////////////////////////
	
	public Night.Interface.ResourceStream.Writer? request_writer_for_downloading(string resource_id, string resource_name){
		return callback_handler.request_writer_for_downloading(resource_id, resource_name);
	}
	
	public Night.Interface.ResourceStream.Reader? request_reader_for_uploading(string resource_id, string resource_name){
		return callback_handler.request_reader_for_uploading(resource_id, resource_name);
	}
}
