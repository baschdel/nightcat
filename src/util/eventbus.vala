public class Night.Util.Eventbus : Object {
	
	public string syncid { get; protected set; }
	
	public signal void event(string syncids, string emitter, string type, string content_type, string data);
	
	public Eventbus(string? syncid = null){
		if(syncid != null) {
			this.syncid = syncid;
		} else {
			this.syncid = GLib.Uuid.string_random();
		}
	}
	
	public void on_external_event(string syncids, string emitter, string type, string content_type, string data) {
		if (syncid in syncids.split(";")) { return; }
		event(@"$syncids;$syncid",emitter,type,content_type,data);
	}
	
	public void emit(string emitter, string type, string content_type, string data) {
		event(syncid,emitter,type,content_type,data);
	}
	
}
