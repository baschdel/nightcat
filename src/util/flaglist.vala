public class Night.Util.Flaglist : Object {
	protected List<string> flags = new List<string>();
	
	public void set_flag(string flag, bool state = true){
		lock (flags) {
			if (state) {
				lock(flags) {
					if (!has_flag(flag)){
						flags.append(flag);
					}
				}
			} else {
				this.clear_flag(flag);
			}
		}
	}
	
	public void clear_flag(string flag){
		lock (flags) {
			if (has_flag(flag)){
				unowned List<string>? link = flags.find_custom(flag,(a,b) => {
					if(a==b){ return 0; }
					return 1;
				});
				flags.delete_link(link);
			}
		}
	}
	
	public bool has_flag(string flag){
		lock (flags) {
			foreach( string f in flags ) {
				if (f==flag) {return true;}
			}
			return false;
		}
	}
	
	public string[] list_flags(){
		lock (flags) {
			string[] list = new string[flags.length()];
			uint i = 0;
			foreach(string flag in flags) {
				if (i > list.length){ break; }
				list[i] = flag;
				i++;
			}
			return list;
		}
	}
	
	public void foreach(Func<string> cb){
		foreach(string flag in flags) {
			cb(flag);
		}
	}
		
}
