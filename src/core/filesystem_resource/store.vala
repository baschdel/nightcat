public class Night.FilesystemResource.Store : Night.Interface.Resource.Store, Object {
	
	public string directory { get; protected set; }
	private string uuid = GLib.Uuid.string_random();
	
	private HashTable<string,Resource> resources = new HashTable<string,Resource>(str_hash,str_equal);
	
	private bool is_temporary;
	
	public Night.Interface.Log.Logger? logger {
		get {
			return log.logger;
		}
		set {
			log.set_backend_logger(value);
		}
	}
	
	private Night.ModuleHelper.Log.Loginput log = new Night.ModuleHelper.Log.Loginput("filesystem_resource_store");
	
	public Store(string directory, bool is_temporary, Night.Interface.Log.Logger? logger = null){
		this.logger = logger;
		this.is_temporary = is_temporary;
		log.info("_!!!_night.filesystem_resource.store:constructing");
		this.directory = directory;
		if (!directory.has_suffix("/")){
			this.directory += "/";
		}
		GLib.DirUtils.create_with_parents(this.directory,16832);
		log.info("_!!!_night.filesystem_resource.store:directory_created…"+this.directory.escape());
	}
	
	~Store(){
		if (is_temporary) {
			log.info("_!!!_night.filesystem_resource.store:decontructing");
			nuke(); //WARNING: Only do this wit temporary files
			GLib.DirUtils.remove(this.directory);
			log.info("_!!!_night.filesyste_resource.store:directory_removed…"+this.directory.escape());
		}
	}
	
	public string get_store_uuid(){
		return uuid;
	}
	
	public bool writable(){
		return true;
	}
	
	public bool has_resorce(string name){
		return name in resources;
	}
	
	public Night.Interface.Resource.Resource? retrieve_resource(string name){
		return resources.get(name);
	}
	
	public Night.Interface.Resource.Writer? retrieve_resource_writer(string name){
		var resource = resources.get(name);
		if (resource == null) { return null; }
		return resource.get_writer();
	}
	
	public bool create_resource(string name){
		log.info("_!!!_night.filesystem_resource.store:resource_created…"+directory+GLib.Uri.escape_string(name));
		var resource = new Resource(directory+GLib.Uri.escape_string(name), true, log, uuid+":"+name);
		if (!resource.exists()) { return false; }
		resource.file_deleted.connect(() => {
			this.resource_deleted(name);
		});
		resources.set(name,resource);
		return true;
	}
	
	public bool delete_resource(string name){
		var resource = resources.get(name);
		if (resource == null) { return true; }
		if (resource.delete()) {
			return true;
		} else {
			return false;
		}
	}
	
	private void resource_deleted(string name){
		resources.remove(name);
		log.info("_!!!_night.filesystem_resource.store:resource_deleted…"+directory+GLib.Uri.escape_string(name));
	}
	
	public bool is_indexable(){
		return true;
	}
	
	public void foreach_resource(Func<string> cb){
		resources.foreach((name,_) => {
			cb(name);
		});
	}
	
	public uint32 count_resources(){
		return resources.size();
	}
	
	public uint64 total_size(){
		return 0;
	}
	
	public uint64 free_size(){
		return 0;
	}
	
	public void nuke(){
		this.resources.foreach_remove((name,resource) => {
			resource.delete();
			return true;
		});
	}
}
