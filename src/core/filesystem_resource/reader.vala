public class Night.FilesystemResource.Reader : Night.Interface.ResourceStream.Reader, Night.Interface.Resource.Reader, Object {
	private Night.FilesystemResource.Resource _resource;
	private FileInputStream? input_stream = null;
	private bool closed = false;
	
	public Reader(Night.FilesystemResource.Resource resource){
		this._resource = resource;
		this._resource.file_updated.connect(on_resource_updated);
	}
	
	~Reader(){
		this.close();
	}
	
	
	private void on_resource_updated(bool append_only){
		if (!append_only){
			this.close_input_stream();
		}
		this.updated(append_only);
		this.data_avaiable();
	}
	
	private void close_input_stream(){
		lock(input_stream){
			if (input_stream != null) {
				try {
					input_stream.close();
				} catch (Error e) {
					//do nothing as this can't be fixed
				}
				input_stream = null;
			}
		}
	}
	
	//designed to be wrapped inside a lock(input_stream)
	private bool open_input_stream(){
		if (closed) { return false; }
		if (input_stream == null) {
			try{
				input_stream = _resource.file.read();
				return true;
			} catch (Error e) {
				return false;
			}
		} else {
			return true;
		}
	}
	
	  /////////////////////////////////////
	 // Night.Interface.Resource.Reader //
	/////////////////////////////////////
	
	public void close(){
		lock(closed){
			this.closed = true;
			this._resource.file_updated.disconnect(on_resource_updated);
			this.close_input_stream();
		}
	}
	
	public uint8[]? read(uint64 maxlen = 100000, Cancellable? cancellable = null){
		lock(input_stream){
			return _read(maxlen);
		}
	}
	
	private uint8[]? _read(uint64 maxlen = 100000, Cancellable? cancellable = null){
		if (open_input_stream()) {
			try {
				return Bytes.unref_to_data(input_stream.read_bytes((size_t) maxlen));
			} catch (Error e) {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public uint8[]? peek(uint64 maxlen = 10000, Cancellable? cancellable = null){
		if (closed) { return null; }
		lock(input_stream){
			var pos = _get_current_position();
			var data = _read(maxlen);
			aseek(pos);
			return data;
		}
	}
	
	public uint64 get_current_position(){
		if (closed) { return 0; }
		lock(input_stream){
			return _get_current_position();
		}
	}
	
	private uint64 _get_current_position(){
		if (input_stream != null) {
			return input_stream.tell();
		} else {
			return 0;
		}
	}
	
	public uint64 get_current_size(){
		if (closed) { return 0; }
		lock(input_stream){
			return _get_current_size();
		}
	}
	
	private uint64 _get_current_size(){
		if (input_stream != null){
			try {
				var fileinfo = this.input_stream.query_info("standard::size");
				return fileinfo.get_size();
			} catch (Error e) {
				return _resource.get_current_size();
			}
		} else {
			return _resource.get_current_size();
		}
	}
	
	public bool has_data(){	
		if (closed) { return true; }
		lock(input_stream){
			return _get_current_position() < _get_current_size();
		}
	}
	
	public bool is_closed(){
		return closed;
	}
	
	public bool is_backward_seekable(){
		return true;
	}
	
	public uint64 aseek(uint64 abspos){ //trys to seek to to the new position in the file, returns new position
		if (closed) { return 0; }
		lock(input_stream){
			if (!open_input_stream()){
				return 0;
			}
			uint64 pos = uint64.min(this._get_current_size(),abspos);
			try {
				input_stream.seek((int64) pos,GLib.SeekType.SET);
			} catch (Error e) {
				//do nothing and return position
			}
			return input_stream.tell();
		}
	}
		
	public Night.Interface.Resource.Resource get_resource(){
		return this._resource;
	}
}
