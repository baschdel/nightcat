public delegate string? Night.ModuleHelper.Register.ReadSingle();
public delegate bool Night.ModuleHelper.Register.WriteSingle(string val);

public class Night.ModuleHelper.Register.Register : Object {
	public Night.ModuleHelper.Register.ReadSingle? read_function = null;
	public Night.ModuleHelper.Register.WriteSingle? write_function = null;
	
	public Register(owned Night.ModuleHelper.Register.ReadSingle? read_function, owned Night.ModuleHelper.Register.WriteSingle? write_function){
		this.read_function = (owned) read_function;
		this.write_function = (owned) write_function;
	}
}

public class Night.ModuleHelper.Register.ReportItemProvider : Night.ModuleHelper.Report.ItemProvider, Object {
	public Night.ModuleHelper.Register.Register _register;
	public string name;
	
	public ReportItemProvider(string name, Night.ModuleHelper.Register.Register register){
		this.name = name;
		this._register = register;
	}
	
	public string? get_report_value(){
		if (_register.read_function != null) {
			return _register.read_function();
		} else {
			return null;
		}
	}
	
	public string get_report_name(){
		return name;
	}
}

public class Night.ModuleHelper.Register.Manager : Object {
	
	protected HashTable<string,Night.ModuleHelper.Register.Register> registers = new HashTable<string,Night.ModuleHelper.Register.Register>(str_hash, str_equal);
	
	public void foreach_register(Func<string> cb){
		registers.foreach((k,v) => {
			cb(k);
		});
	}
	
	public bool write_register(string register_id, string val){
		var register = registers.get(register_id);
		if (register != null) {
			if (register.write_function != null) {
				return register.write_function(val);
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public string? read_register(string register_id){
		var register = registers.get(register_id);
		if (register != null) {
			if (register.read_function != null) {
				return register.read_function();
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
}
