public interface Night.ModuleHelper.Report.ItemProvider : Object {
	public abstract string? get_report_value();
	public abstract string get_report_name();
}

public class Night.ModuleHelper.Report.Generator {
	
	protected List<Night.ModuleHelper.Report.ItemProvider > status_report = new List<Night.ModuleHelper.Report.ItemProvider >();

	public string get_status_report(){
		var report = new Night.Util.Kv();
		foreach(Night.ModuleHelper.Report.ItemProvider provider in status_report){
			string? val = provider.get_report_value();
			if (val != null){
				report.set_value(provider.get_report_name(),provider.get_report_value());
			}
		}
		return report.export();
	}
}
