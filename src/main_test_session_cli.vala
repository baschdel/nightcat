public static int main(string[] args) {
	var log = new Night.ModuleHelper.Log.Loginput("NightCat");
	log.print_log = true;
	
	var session = new Night.Backend.Session.SimpleSession.Session();
	var gopher_fetcher = new Night.Backend.Protocol.Gopher.Fetcher();
	var gemini_fetcher = new Night.Backend.Protocol.Gemini.Fetcher();
	var finger_fetcher = new Night.Backend.Protocol.Finger.Fetcher();
	var connector = new Night.Backend.Connector.TcpIp();
	var resolver = new Night.Backend.Resolver.GLibDnsResolver();
	var tls_resolver = new Night.Backend.Resolver.TlsHostResolver();
	
	session.multithreaded = true;
	session.set_fetcher(gopher_fetcher,"gopher");
	session.set_fetcher(gemini_fetcher,"gemini");
	session.set_fetcher(finger_fetcher,"finger");
	session.set_connector(connector);
	session.host_resolver_configuration.add_alias("","tcpip");
	session.host_resolver_configuration.set_resolver("glib_dns_resolver", "dns", Night.Interface.Request.NameCategory.NEUTRAL, resolver);
	session.host_resolver_configuration.set_resolver("tls_resolver", "tls", Night.Interface.Request.NameCategory.NEUTRAL, tls_resolver);
	session.host_resolver_configuration.add_rule("tcpip","dns");
	session.host_resolver_configuration.add_rule("tls+","tls");
	session.authority_resolver_configuration.add_rule("finger+","*","79");
	session.authority_resolver_configuration.add_rule("gopher+","*","70");
	session.authority_resolver_configuration.add_rule("gemini","tls","1965");
	session.authority_resolver_configuration.add_rule("gopher","tls","7070",null,".cosmic.voyage");
	session.authority_resolver_configuration.add_rule("gopher","tls","105",null,".khzae.net");
	//session.authority_resolver_configuration.add_rule("gopher","tls","7070",null,null, Night.Interface.Request.NameCategory.PREFERRED);
	//session.authority_resolver_configuration.add_rule("gopher","tls","105",null,null, Night.Interface.Request.NameCategory.PREFERRED);
	
	Mutex exit = Mutex();
	var context = new RequestContext();
	context.done.connect(() => {
		exit.unlock();
	});
	session.request_download(context, "gopher://cosmic.voyage/");
	exit.lock();
	exit.lock();
	return 0;
}

private class RequestContext : Night.Interface.Request.RequestContext, Object {
	public Night.Interface.Request.Request? request = null;
	
	public signal void done();
	
	public void set_request_object(Night.Interface.Request.Request request){
		this.request = request;
	}
	
	public Night.Interface.ResourceStream.Writer? request_resource_download_destination(string context_uuid, string uri){
		return new Writer(uri);
	}
	
	public void append_log_entry(Night.Interface.Request.Log.Entry entry){
		print(serialize_log_entry(entry,request)+"\n");
		if (!request.is_root_context_open()) {
			done();
		}
	}
}

private class Writer : Night.Interface.ResourceStream.Writer, Object {
	private bool closed = false;
	private string uri;
	
	public Writer(string uri){
		this.uri = uri;
	}

	public bool close(){
		closed = true;
		return true;
	}
	
	public bool is_closed(){
		return closed;
	}

	public bool set_attribute(string key, string val){
		return !closed;
	}
	
	public bool append(uint8[] data, Cancellable? cancellable = null){
		print(@"$(data.length) bytes appended to download from $uri\n");
		return !closed;
	}
	public bool can_write(){ return true; }
}

public static string serialize_log_entry(Night.Interface.Request.Log.Entry entry, Night.Interface.Request.Request request){
	var json_array = new Json.Array.sized(4);
	json_array.add_string_element(entry.get_severity().to_string());
	json_array.add_string_element(entry.get_event_type());
	json_array.add_string_element(entry.get_context_id());
	var arguments = new Json.Object();
	entry.foreach_argument((k) => {
		string? v = entry.get_argument(k);
		if (v == null) { 
			arguments.set_null_member(k);
		} else {
			arguments.set_string_member(k,v);
		}
	});
	json_array.add_object_element(arguments);
	var node = new Json.Node(Json.NodeType.ARRAY);
	node.init_array(json_array);
	return Json.to_string(node,false);
}

public static string serialize_log_entry_old(Night.Interface.Request.Log.Entry entry, Night.Interface.Request.Request request){
	string s = "\n"; // entry.get_unlocalized_message()+"\n";
	string? name = request.get_context_name(entry.get_context_id());
	if (name == null) { name = "[no name]"; }
	s += @"[$(entry.get_severity())] $(entry.get_event_type()) from $(name) ($(entry.get_context_id()))\n";
	entry.foreach_argument((k) => {
		string? v = entry.get_argument(k);
		if (v == null) { 
			s += @"$k is null\n";
		} else {
			s += @"$k = $v\n";
		}
	});
	if (entry.get_event_type() == "context_created") {
		s += "[configuration]\n";
		var configuration = request.get_context_configuration(entry.get_context_id());
		if (configuration == null) {
			s += "NO CONFIGURATION FOUND\n";
		} else {
			configuration.foreach_key((key) => {
				var val = configuration.get_value(key);
				if (val != null) {
					s += @"$key = $val\n";
				} else {
					s += @"$key is null\n";
				}
			});
		}
	}
	return s;
}

